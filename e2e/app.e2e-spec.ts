import { OctoPostPage } from './app.po';

describe('octo-post App', () => {
  let page: OctoPostPage;

  beforeEach(() => {
    page = new OctoPostPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
