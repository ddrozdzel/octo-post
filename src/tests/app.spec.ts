import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, ViewContainerRef, NO_ERRORS_SCHEMA } from '@angular/core';

import { Observable } from 'rxjs';
import "rxjs/add/observable/of";


import { AuthLoginDouble } from  './test-helpers/authLoginDouble';
import { RouterLinkStubDirective } from './test-helpers/routerLinkStubDirective';
import { ErrorDouble } from './test-helpers/errorDouble';
import { click } from './test-helpers/clickHelper';

import { ErrorsComponent } from  './../app/errors.component';
import { ErrorsService } from './../app/services/errors.service';
import { AppComponent } from './../app/app';
import { AuthLoginService } from './../app/services/auth-login.service';


describe('App Component test', () => {
  let fixture: ComponentFixture<AppComponent>;
  let comp:  AppComponent;
  let authLoginService: any;
  let spyErrorService: any;
  let linkDirectives: any;
  let links: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent, RouterLinkStubDirective, ErrorsComponent ],
      providers: [
        { provide: AuthLoginService, useFactory: () => { return new AuthLoginDouble() } },
        ViewContainerRef,
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;
    authLoginService = TestBed.get(AuthLoginService);

    linkDirectives = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective));
    links = linkDirectives.map(de => de.injector.get(RouterLinkStubDirective) as RouterLinkStubDirective)
    fixture.detectChanges();
  });


  it("should login and logout user", ()=> {
    expect(comp.user).toBeUndefined();
    authLoginService.logUser();
    fixture.detectChanges();
    expect(comp.user).toBe('user123');

    let userSpan = fixture.debugElement.query(By.css('span')).nativeElement;
    expect(userSpan.innerText).toBe('user123');

    authLoginService.logout();
    fixture.detectChanges();

    expect(comp.user).toBe('');
    let userSpan2 = fixture.debugElement.query(By.css('span')).nativeElement;
    expect(userSpan2.innerText).toBe('');

  });


  it("should check router links in template", () => {
    expect(links.length).toBe(6);
    expect(links[0].linkParams).toContain('/login');
    expect(links[1].linkParams).toContain('/dashboard');
    expect(links[2].linkParams).toContain('/addpost');
    expect(links[3].linkParams).toContain('/reports');
    expect(links[4].linkParams).toContain('/socials');
    expect(links[5].linkParams).toContain('/config');
  });

  it("should check routing navigation", () =>  {
    let linkDest: string[] = ['','/dashboard', '/addpost', '/reports', '/socials', '/config' ];

    for(var i = 1; i <= 5; i++) {
      expect(links[i].navigatedTo).toBeNull();
      click(linkDirectives[i]);
      fixture.detectChanges();
      expect(links[i].navigatedTo).toContain(linkDest[i]);
    }
  });




});
