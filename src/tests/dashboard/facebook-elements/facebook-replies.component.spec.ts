import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, SimpleChange }    from '@angular/core';


import { FacebookRepliesComponent } from './../../../app/dashboard/facebook-elements/replies/facebook-replies.component';
import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookPostsService } from './../../../app/services/facebook-posts.service';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { FacebookPostsDummy } from './../../test-helpers/facebookDummyServices';
import { click } from './../../test-helpers/clickHelper';

describe('Facebook Replies Component test', () => {
  let fixture: ComponentFixture<FacebookRepliesComponent>;
  let comp:  FacebookRepliesComponent;
  let serviceInstance: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacebookRepliesComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookPostsService, useFactory: () => { return new FacebookPostsDummy() } },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookRepliesComponent);
    comp = fixture.componentInstance;
    serviceInstance = TestBed.get(FacebookPostsService);
    spyOn(serviceInstance, 'getReplies').and.callThrough();
    spyOn(serviceInstance, 'addReplie').and.callThrough();
    spyOn(serviceInstance, 'addLike').and.callThrough();
    comp.showReplies(1,2);
    fixture.detectChanges();
  });


  it("should test if component is loading all values",  async(() => {
    let repliesStubs = serviceInstance.getRepliesStub();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(comp.replies.length).toBe(2);
      expect(comp.replies[0]).toEqual(repliesStubs[0]);
      expect(comp.replies[1]).toEqual(repliesStubs[1]);
      expect(serviceInstance.getReplies).toHaveBeenCalledWith(1,2);
      expect(comp.idInfo).toEqual({postId:1, anwserId:2});
    });
  }));

  it("should test if template is rednered",   async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
        let imgs = fixture.debugElement.queryAll(By.css('img'));
        expect(imgs[0].nativeElement.src).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
        expect(imgs[1].nativeElement.src).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");

        let spans = fixture.debugElement.queryAll(By.css('.replieUser'));
        expect(spans[0].nativeElement.innerText).toBe('Bożena');
        expect(spans[1].nativeElement.innerText).toBe('Janusz');

        let awnsers = fixture.debugElement.queryAll(By.css('.anwserReplie'));
        expect(awnsers[0].nativeElement.innerText).toBe("O ty pierunie, wylaz z neta?");
        expect(awnsers[1].nativeElement.innerText).toBe('Ale bożena, jak to?');

        let likes = fixture.debugElement.queryAll(By.css('b'));
        expect(likes[0].nativeElement.innerText).toBe('2');
        expect(likes[1].nativeElement.innerText).toBe('0');
      });
  }));

  it("should test makeReplie",   async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
        let textArea = fixture.debugElement.query(By.css('textarea'));
        textArea.nativeElement.value = "replie";
        textArea.triggerEventHandler('keyup.enter', null);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          expect(serviceInstance.addReplie).toHaveBeenCalledWith({postId:1, anwserId:2}, 'replie');
          expect(comp.replies.length).toBe(3);
          expect(comp.replies[2]).toEqual({date: "now",user: "ja",userPic: "https://65.media.tumblr.com/avatar_aa8f82d10a6d_128.png",msg: "replie",likes: 0});
          let textArea2 = fixture.debugElement.query(By.css('textarea')).nativeElement;
          expect(textArea2.value).toBe('');
        });
      });
  }));

  it("should test replieLike",   async(() => {
    spyOn(comp, "addReplieLike").and.callThrough();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      let likes = fixture.debugElement.queryAll(By.css('b'));
      expect(likes[0].nativeElement.innerText).toBe('2');

      let likesButton = fixture.debugElement.queryAll(By.css('span[role="button"]'));
      click(likesButton[0]);
      fixture.detectChanges();;
      expect(comp.addReplieLike).toHaveBeenCalledWith(1);
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(serviceInstance.addLike).toHaveBeenCalledWith(1,2,1);
        let likes2 = fixture.debugElement.queryAll(By.css('b'));
        expect(likes2[0].nativeElement.innerText).toBe('3');

        click(likesButton[0]);
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
            expect(serviceInstance.addLike).toHaveBeenCalledWith(1,2,1);
            let likes2 = fixture.debugElement.queryAll(By.css('b'));
            expect(likes2[0].nativeElement.innerText).toBe('2');
        });

      });
      });
  }));



});
