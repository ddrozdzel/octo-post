import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, SimpleChange }    from '@angular/core';


import { FacebookLikesComponent } from './../../../app/dashboard/facebook-elements/likes/facebook-likes.component';
import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookLikesService } from './../../../app/services/facebook-likes.service';
import { FacebookInteractUsers } from '../../../app/shared/models/facebook-interact-users';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { LikesSharesDummy } from './../../test-helpers/facebookDummyServices';
import { click } from './../../test-helpers/clickHelper';


describe('Facebook Likes Component test', () => {
  let fixture: ComponentFixture<FacebookLikesComponent>;
  let comp:  FacebookLikesComponent;
  let likesStub: any;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacebookLikesComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookLikesService, useFactory: () => { return new LikesSharesDummy() } },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookLikesComponent);
    comp = fixture.componentInstance;
    let service = TestBed.get(FacebookLikesService);
    likesStub = service.getLikesShares();

    comp.selectedPostLikes =  {id: 2,date: "3 hours ago",msg: "tekst",likes: 0, shares: 2,anwsers: []};
    comp.ngOnChanges(comp.selectedPostLikes);
    fixture.detectChanges();
  });

  it('should get likes from api', async(()=> {
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
      expect(comp.facebookInteractUsers.length).toBe(2);
      expect(comp.facebookInteractUsers[0]).toEqual(likesStub[0]);
    });
  }));

  it('should render template', async(()=> {
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
      let imgs = fixture.debugElement.queryAll(By.css('img'));
      let li = fixture.debugElement.queryAll(By.css('li'));
      expect(li[0].nativeElement.innerText).toBe(likesStub[0].user);
      expect(imgs[0].nativeElement.src).toContain(likesStub[0].userPic);
      expect(li[1].nativeElement.innerText).toBe(likesStub[1].user);
      expect(imgs[1].nativeElement.src).toContain(likesStub[1].userPic);
    });
  }));

  it("should disable likes pop-up by clicking",  async(()=> {
    spyOn(comp, 'disableSelectedPost').and.callThrough();
    spyOn(comp.disableLikes, 'emit');
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
        let div = fixture.debugElement.query(By.css('div'));
        click(div);
        fixture.detectChanges();
        expect(comp.disableSelectedPost).toHaveBeenCalled();
        expect(comp.disableLikes.emit).toHaveBeenCalled();
    });
  }));
});
