import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FacebookPostShareComponent } from './../../../app/dashboard/facebook-elements/post-share/facebook-post-share.component';
import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookPostShareService } from './../../../app/services/facebook-post-share.service';
import { AccordionModule, ModalModule } from 'ng2-bootstrap';
import { SocialLoginSearchPipe } from './../../../app/shared/social-login-search.pipe';
import { SocialStreamChooserComponent } from './../../../app/shared/components/social-stream-chooser/social-stream-chooser.component';
import { SocialsService } from './../../../app/services/socials.service';
import { SocialsServiceDummy } from './../../test-helpers/socialsServiceDummy';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { FbPostShareDummy } from './../../test-helpers/facebookDummyServices';
import { click } from './../../test-helpers/clickHelper';
import {AccordionAccountComponent } from './../../../app/shared/components/accordion-account/accordion-account.component';



describe("Facebook Post Share Component test", () => {
  let fixture: ComponentFixture<FacebookPostShareComponent>;
  let comp:  FacebookPostShareComponent;
  let sharePostStub: any = {id:1, msg:"test text"}
  const socialAccountStub = {id:1, social: "twitter", userPic: 'none', login: 'twitterUser', active: true };
  const socialAccountStub2 = {id:2, social: "facebook", userPic: 'none', login: 'facebookUser', active: true };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, AccordionModule.forRoot(), ModalModule.forRoot()],
      declarations: [FacebookPostShareComponent, SocialStreamChooserComponent, SocialLoginSearchPipe, AccordionAccountComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookPostShareService, useFactory: () => { return new FbPostShareDummy() } },
        { provide: SocialsService, useFactory: () => { return new SocialsServiceDummy() } },
        FormBuilder
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookPostShareComponent);
    comp = fixture.componentInstance;
    comp.sharePost = sharePostStub;
    comp.ngOnChanges(comp.sharePost);
    fixture.detectChanges();
  });

  it('should test if all values are propery loaded', ()=> {
    expect(comp.privacies).toEqual(["Only Me", "Friends", "Friends of My Friends", "Public", "Other"]);
    expect(Object.keys(comp.accountsStorage).length).toBe(0);
    expect(comp.newSharedPost.controls['comment'].value).toBe('');
    expect(comp.newSharedPost.controls['privacy'].value).toBe('');
    expect(comp.choosedAccounts.length).toBe(0);
  });

  it('should test if template is rendered', ()=> {
    const options = fixture.debugElement.queryAll(By.css('option'));
    expect(options.length).toBe(5);
    expect(options[0].nativeElement.value).toBe("Only Me");

    const h4 = fixture.debugElement.queryAll(By.css('h4'));
    expect(h4[1].nativeElement.innerText).toBe("test text");
  });

  describe('...testing validators...', () => {
    it('should test if privacies input value is proper validated',() => {
       expect(comp.newSharedPost.get('privacy').hasError('required')).toBeTruthy();
       let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
       privacyInput.value = "Only Me";
       privacyInput.checked = true;
       privacyInput.dispatchEvent(new Event('change'));
       fixture.detectChanges();
       expect(comp.newSharedPost.get('privacy').hasError('required')).toBeFalsy();
    });

    it('should test if comment input value is proper validated',() => {
       expect(comp.newSharedPost.get('comment').hasError('maxlength')).toBeFalsy();
       let commentInput = fixture.debugElement.query(By.css('#Comment')).nativeElement as HTMLInputElement;
       commentInput.value = "some comment";
       commentInput.dispatchEvent(new Event('input'));
       fixture.detectChanges();
       expect(comp.newSharedPost.get('privacy').hasError('maxlength')).toBeFalsy();
    });

    it('should test if input accounts empty values is proper validated', () => {
      spyOn(comp.newSharedPost, 'patchValue').and.callThrough();
      expect(comp.newSharedPost.get('account').hasError('isEmptyArray')).toBeTruthy();
      spyOn(comp, 'removeAccountFromChoosed').and.callThrough();
      spyOn(comp.socialChooser, 'addToUsedSocials');

      comp.selectAccount(socialAccountStub);
      fixture.detectChanges();


      let accordions = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(comp.newSharedPost.get('account').hasError('isEmptyArray')).toBeFalsy();

      expect(accordions.length).toBe(1);
      expect(accordions[0].nativeElement.innerText).toContain('twitterUser');
      expect(Object.keys(comp.accountsStorage).length).toBe(1);
      expect(comp.accountsStorage[1]).toBe('twitter');


      comp.selectAccount(socialAccountStub2);
      fixture.detectChanges();

      expect(comp.newSharedPost.get('account').hasError('isEmptyArray')).toBeFalsy();
      let accordions2 = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(accordions2.length).toBe(2);
      expect(accordions2[0].nativeElement.innerText).toContain('twitterUser');
      expect(accordions2[1].nativeElement.innerText).toContain('facebookUser');
      expect(comp.newSharedPost.patchValue).toHaveBeenCalledTimes(2);
      expect(Object.keys(comp.accountsStorage).length).toBe(2);
      expect(comp.accountsStorage[1]).toBe('twitter');
      expect(comp.accountsStorage[2]).toBe('facebook');

      let removeButton = fixture.debugElement.queryAll(By.css('accordion-group button'));
      click(removeButton[0]);
      fixture.detectChanges();
      expect(comp.removeAccountFromChoosed).toHaveBeenCalledWith(socialAccountStub);
      let accordions3 = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(accordions3.length).toBe(1);
      expect(accordions3[0].nativeElement.innerText).toContain('facebookUser');
    });


    describe('submit form', ()=> {
      let privacyInput:HTMLInputElement;
      let commentInput: HTMLInputElement;
      let form: any;
      let serviceSpy: any;

      beforeEach(() => {
        spyOn(comp.postShared, 'emit');

        serviceSpy = TestBed.get(FacebookPostShareService);
        spyOn(serviceSpy, 'shareFacebookPost').and.callThrough();

        form = fixture.debugElement.query(By.css('form')).nativeElement;

        privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
        privacyInput.value = "Only Me";
        privacyInput.checked = true;
        privacyInput.dispatchEvent(new Event('change'));

        commentInput = fixture.debugElement.query(By.css('#Comment')).nativeElement as HTMLInputElement;
        commentInput.value = "some comment";
        commentInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();
      });

      it('will be submitted', async(() => {
        expect(comp.newSharedPost.invalid).toBeTruthy();
        comp.selectAccount(socialAccountStub);
        comp.selectAccount(socialAccountStub2);

        fixture.detectChanges();

        form.dispatchEvent(new Event('submit'));
        fixture.detectChanges();
        expect(comp.newSharedPost.invalid).toBeFalsy();
          fixture.whenStable().then(() => {
             fixture.detectChanges();
             expect(serviceSpy.shareFacebookPost).toHaveBeenCalledWith({postId:1, account: {1: 'twitter', 2: 'facebook'} , comment:'some comment', privacy:"Only Me" });
             expect(comp.postShared.emit).toHaveBeenCalled();
           }
         );
       }));

       it('wont be submitted', async(() => {
         expect(comp.newSharedPost.invalid).toBeTruthy();

         fixture.detectChanges();

         form.dispatchEvent(new Event('submit'));
         fixture.detectChanges();
         expect(comp.newSharedPost.invalid).toBeTruthy();
        }));

    });

  });




});
