import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, SimpleChange }    from '@angular/core';

import { FacebookSharesComponent } from './../../../app/dashboard/facebook-elements/shares/facebook-shares.component';
import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookSharesService } from './../../../app/services/facebook-shares.service';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { click } from './../../test-helpers/clickHelper';
import { LikesSharesDummy } from './../../test-helpers/facebookDummyServices';

describe('Facebook Shares Component test', () => {
  let fixture: ComponentFixture<FacebookSharesComponent>;
  let comp:  FacebookSharesComponent;
  let likesStub: any;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacebookSharesComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookSharesService, useFactory: () => { return new LikesSharesDummy() } },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookSharesComponent);
    comp = fixture.componentInstance;
    let service = TestBed.get(FacebookSharesService);
    likesStub = service.getLikesShares();

    comp.selectedPostShares =  {id: 2,date: "3 hours ago",msg: "tekst",likes: 0, shares: 2,anwsers: []};
    comp.ngOnChanges(comp.selectedPostShares);
    fixture.detectChanges();
  });

  it('should get shares from api', async(()=> {
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
      expect(comp.facebookInteractUsers.length).toBe(2);
      expect(comp.facebookInteractUsers[0]).toEqual(likesStub[0]);
    });
  }));

  it('should render template', async(()=> {
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
      let imgs = fixture.debugElement.queryAll(By.css('img'));
      let li = fixture.debugElement.queryAll(By.css('li'));
      expect(li[0].nativeElement.innerText).toBe(likesStub[0].user);
      expect(imgs[0].nativeElement.src).toContain(likesStub[0].userPic);
      expect(li[1].nativeElement.innerText).toBe(likesStub[1].user);
      expect(imgs[1].nativeElement.src).toContain(likesStub[1].userPic);
    });
  }));

  it("should disable likes pop-up by clicking",  async(()=> {
    spyOn(comp, 'disableSelectedPost').and.callThrough();
    spyOn(comp.disableShares, 'emit');
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      fixture.detectChanges();
        let div = fixture.debugElement.query(By.css('div'));
        click(div);
        fixture.detectChanges();
        expect(comp.disableSelectedPost).toHaveBeenCalled();
        expect(comp.disableShares.emit).toHaveBeenCalled();
    });
  }));
});
