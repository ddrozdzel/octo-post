import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { FacebookEventsComponent } from './../../../app/dashboard/facebook-elements/events/facebook-events.component';
import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookEventsService } from './../../../app/services/facebook-events.service';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { EventsDummy } from './../../test-helpers/facebookDummyServices';


describe("Facebook events test", () => {
  let fixture: ComponentFixture<FacebookEventsComponent>;
  let comp:  FacebookEventsComponent;
  let eventsStub: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FacebookEventsComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookEventsService, useFactory: () => { return new EventsDummy() } },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookEventsComponent);
    comp = fixture.componentInstance;
    let service = TestBed.get(FacebookEventsService);
    eventsStub = service.getEventsStub();
  });

  it('should get events from api', async(()  => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(comp.facebookEvents.length).toBe(2);
      expect(comp.facebookEvents[0]).toEqual(eventsStub[0]);
    });
  }));


  it('should check if events are rendered in template', async(()  => {
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      let h4 = fixture.debugElement.query(By.css('h5')).nativeElement;
      expect(h4.innerText).toBe('Koncert Łąki Łan');

      let p = fixture.debugElement.queryAll(By.css('p'));
      expect(p[0].nativeElement.innerText).toContain('06-12-2016 16:00');
      expect(p[1].nativeElement.innerText).toContain('Łąki Łan');

      let a = fixture.debugElement.query(By.css('a')).nativeElement;
      expect(a.href).toBe('https://www.facebook.com/events/255962424790662');
    });
  }));




})
