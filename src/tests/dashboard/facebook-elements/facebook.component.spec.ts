import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';

import { FacebookComponent } from './../../../app/dashboard/facebook-elements/facebook/facebook.component';
import { ModalModule } from 'ng2-bootstrap';
import { SafeUrlPipe } from './../../../app/shared/safe-url.pipe';

import { ErrorsService } from './../../../app/services/errors.service';
import { FacebookPostsService } from './../../../app/services/facebook-posts.service';
import { ErrorDouble } from './../../test-helpers/errorDouble';
import { FacebookPostsDummy } from './../../test-helpers/facebookDummyServices';
import { click } from './../../test-helpers/clickHelper';



describe('Facebook Component test', () => {
  let fixture: ComponentFixture<FacebookComponent>;
  let comp:  FacebookComponent;
  const socialAccountStub = {id:1, social: "facebook", userPic: 'none', login: 'facebookUser', active: true };
  let serviceSpy: any;
  let facebookPostsStub: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ModalModule.forRoot()],
      declarations: [FacebookComponent, SafeUrlPipe],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: FacebookPostsService, useFactory: () => { return new FacebookPostsDummy() } },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookComponent);
    comp = fixture.componentInstance;
    serviceSpy = TestBed.get(FacebookPostsService);
    facebookPostsStub = serviceSpy.getFBPostsStub();
    spyOn(serviceSpy, 'getFacebookPosts').and.callThrough();
    comp.facebookAccount = socialAccountStub;
    fixture.detectChanges();
  });

  describe('starting component', ()=> {
    it('should check if all values are properly set', async(()=> {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(serviceSpy.getFacebookPosts).toHaveBeenCalled();
      expect(comp.facebookPosts.length).toBe(2);
      expect(comp.facebookPosts[0]).toEqual(facebookPostsStub[0]);
      expect(comp.facebookPosts[1]).toEqual(facebookPostsStub[1]);
      expect(comp.showFacebookEvents).toBeFalsy();
      expect(comp.facebookLikesPost).toBeUndefined();
      expect(comp.facebookSharesPost).toBeUndefined();
    });
  }));

  it('should check if infinitive scroll work', async(()=> {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(comp.facebookPosts.length).toBe(2);
      comp.onScrollDown();
      fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          expect(comp.facebookPosts.length).toBe(4);
        });
    });
  }));

    it('should render template' , async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();

        let socialIcon = fixture.debugElement.query(By.css('.socialIcons')).nativeElement;
        expect(socialIcon.src).toContain('assets/facebook.png')

        let h3Login = fixture.debugElement.query(By.css('h3 small')).nativeElement;
        expect(h3Login.innerText).toBe('facebookUser');

        let msg = fixture.debugElement.queryAll(By.css('.postMsg p'));
        expect(msg[0].nativeElement.innerText).toBe("Pierwszy wpis na fejse - link do video");

        let roleButtons = fixture.debugElement.queryAll(By.css('b[role="button"]'));
        expect(roleButtons[0].nativeElement.innerText).toBe('4');
        expect(roleButtons[1].nativeElement.innerText).toBe('1');

        let imgs = fixture.debugElement.queryAll(By.css('.img-circle'));
        expect(imgs[0].nativeElement.src).toContain('none');
        expect(imgs[1].nativeElement.src).toBe('https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg');
        expect(imgs[2].nativeElement.src).toBe('https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg');


        let anwserUser = fixture.debugElement.queryAll(By.css('.anwserUser'));
        expect(anwserUser[0].nativeElement.innerText).toBe('Janusz');
        expect(anwserUser[1].nativeElement.innerText).toBe('Janusz 2');

        let postAnwser = fixture.debugElement.queryAll(By.css('.postAnwser'));
        expect(postAnwser[0].nativeElement.innerText).toBe("Co jest kuwa?");
        expect(postAnwser[1].nativeElement.innerText).toBe('Elo, elo');

        let anwserLikes = fixture.debugElement.queryAll(By.css('.anwserLikes'));
        expect(anwserLikes[0].nativeElement.innerText).toBe('2');
        expect(anwserLikes[1].nativeElement.innerText).toBe('1');
      });
    }));
  });

  describe('actions on component', ()=> {
    let buttons: any;
    let bRoleButtons: any;
    let spanRoleButtons: any;
    beforeEach(() => {
      buttons = fixture.debugElement.queryAll(By.css('button'));
      spanRoleButtons = fixture.debugElement.queryAll(By.css('span[role="button"]'));
    });


    it('should open event popup by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(comp, 'showFbEvents').and.callThrough();
        spyOn(comp.showEvents, 'show');
        click(buttons[0]);
        fixture.detectChanges();
        expect(comp.showFbEvents).toHaveBeenCalled();
        expect(comp.showEvents.show).toHaveBeenCalled();
        expect(comp.showFacebookEvents).toBeTruthy();
      });
    }));


    it('should close facebook tab by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(comp, 'closeFacebookTab').and.callThrough();
        spyOn(comp.closeTab, 'emit').and.callThrough();
        click(buttons[1]);
        fixture.detectChanges();
        expect(comp.closeFacebookTab).toHaveBeenCalled();
        expect(comp.closeTab.emit).toHaveBeenCalled();
      });
    }));

    it('should open likes popup by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let bRoleButtons =  fixture.debugElement.queryAll(By.css('b[role="button"]'));
        spyOn(comp, 'showFacebookLikes').and.callThrough();
        spyOn(comp.showLikesModal, 'show');
        click(bRoleButtons[0]);
        fixture.detectChanges();
        expect(comp.showFacebookLikes).toHaveBeenCalledWith(facebookPostsStub[0]);
        expect(comp.showLikesModal.show).toHaveBeenCalled();
        expect(comp.facebookLikesPost).toEqual(facebookPostsStub[0]);
      });
    }));

    it('should open shares popup by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let bRoleButtons =  fixture.debugElement.queryAll(By.css('b[role="button"]'));
        spyOn(comp, 'showFacebookShares').and.callThrough();
        spyOn(comp.showSharesModal, 'show');
        click(bRoleButtons[1]);
        fixture.detectChanges();
        expect(comp.showFacebookShares).toHaveBeenCalledWith(facebookPostsStub[0]);
        expect(comp.showSharesModal.show).toHaveBeenCalled();
        expect(comp.facebookSharesPost).toEqual(facebookPostsStub[0]);
      });
    }));

    it('should open shares popup by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let roleButtons =  fixture.debugElement.queryAll(By.css('span[role="button"]'));
        spyOn(comp, 'sharePost').and.callThrough();
        spyOn(comp.postShareModal, 'show');
        click(roleButtons[1]);
        fixture.detectChanges();
        expect(comp.sharePost).toHaveBeenCalledWith(facebookPostsStub[0]);
        expect(comp.postShareModal.show).toHaveBeenCalled();
        expect(comp.facebookPostShare).toEqual(facebookPostsStub[0]);
      });
    }));

    it('should add Post like by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let roleButtons =  fixture.debugElement.queryAll(By.css('span[role="button"]'));
        spyOn(comp, 'addLike').and.callThrough();
        spyOn(serviceSpy, 'addLike').and.callThrough();
        click(roleButtons[0]);
        fixture.detectChanges();

        expect(comp.addLike).toHaveBeenCalledWith(1);
        expect(serviceSpy.addLike).toHaveBeenCalledWith(1,null, null);
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          let likesInt = fixture.debugElement.queryAll(By.css('b[role="button"]'));
          expect(likesInt[0].nativeElement.innerText).toBe('5');
          click(roleButtons[0]);
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            fixture.detectChanges();
            let likesInt = fixture.debugElement.queryAll(By.css('b[role="button"]'));
            expect(likesInt[0].nativeElement.innerText).toBe('4');
          });
        });
      });
    }));

    it("should test adding comment",   async(() => {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
          expect(comp.facebookPosts[0].anwsers.length).toBe(2);
          spyOn(serviceSpy, 'addComment').and.callThrough();
          let textArea = fixture.debugElement.query(By.css('textarea'));
          textArea.nativeElement.value = "some comment";
          textArea.triggerEventHandler('keyup.enter', null);
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(serviceSpy.addComment).toHaveBeenCalledWith(1, "some comment");
            expect(comp.facebookPosts[0].anwsers.length).toBe(3);
            expect(comp.facebookPosts[0].anwsers[2]).toEqual({id: 3,date: "10 minutes ago",user: "Janusz",userPic: "janusz.png",msg: "ok",likes: 2,replies: false});
            let textArea2 = fixture.debugElement.query(By.css('textarea')).nativeElement;
            expect(textArea2.value).toBe('');
          });
        });
    }));

    it('should add anwser like by clicking', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        let roleButtons =  fixture.debugElement.queryAll(By.css('span[role="button"]'));
        spyOn(comp, 'addAnwserLike').and.callThrough();
        spyOn(serviceSpy, 'addLike').and.callThrough();
        click(roleButtons[2]);

        fixture.detectChanges();

        expect(comp.addAnwserLike).toHaveBeenCalledWith(1,1);
        expect(serviceSpy.addLike).toHaveBeenCalledWith(1,1, null);

        fixture.whenStable().then(() => {
          fixture.detectChanges();
          let likesInt = fixture.debugElement.queryAll(By.css('.anwserLikes'));
          expect(likesInt[0].nativeElement.innerText).toBe('3');
          click(roleButtons[2]);
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            fixture.detectChanges();
            let likesInt = fixture.debugElement.queryAll(By.css('.anwserLikes'));
            expect(likesInt[0].nativeElement.innerText).toBe('2');
          });
        });
      });
    }));
  });

  describe('other isolated methods', () => {

    it('test disableLikes method', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(comp.showLikesModal, 'hide');
        comp.disableLikes();
        fixture.detectChanges();
        expect(comp.showLikesModal.hide).toHaveBeenCalled();
        expect(comp.facebookLikesPost).toBeNull();
      });
    }));

    it('test disableShares method', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(comp.showSharesModal, 'hide');
        comp.disableShares();
        fixture.detectChanges();
        expect(comp.showSharesModal.hide).toHaveBeenCalled();
        expect(comp.facebookSharesPost).toBeNull();
      });
    }));

    it('test onPostShared method', async(()=> {
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        spyOn(comp.postShareModal, 'hide');
        comp.onPostShared();
        fixture.detectChanges();
        expect(comp.postShareModal.hide).toHaveBeenCalled();
        expect(comp.facebookPostShare).toBeNull();
      });
    }));

  });
});
