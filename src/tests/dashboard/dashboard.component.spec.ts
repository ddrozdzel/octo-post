import { ComponentFixture, TestBed, async} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA }    from '@angular/core';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { click } from './../test-helpers/clickHelper';

import { SocialsServiceDummy } from './../test-helpers/socialsServiceDummy';
import { DashboardComponent } from './../../app/dashboard/dashboard-main/dashboard.component';
import { SocialStreamChooserComponent } from './../../app/shared/components/social-stream-chooser/social-stream-chooser.component';
import { SocialLoginSearchPipe } from './../../app/shared/social-login-search.pipe';
import { ErrorDouble } from './../test-helpers/errorDouble';
import { SocialsService } from './../../app/services/socials.service';
import { ErrorsService } from './../../app/services/errors.service';

describe('DashboardComponent test', () => {
  let fixture: ComponentFixture<DashboardComponent>;
  let comp:  DashboardComponent;
  let stubsAccount = [
     {id: 6, social: "facebook", userPic: "photo.jpg", login: "FacebookUser", active: true},
     {id: 7, social: "twitter", userPic: "photo.jpg", login: "TwitterUser", active: true}
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ BrowserAnimationsModule],
      declarations: [ DashboardComponent, SocialStreamChooserComponent, SocialLoginSearchPipe],
      providers: [
        { provide: SocialsService, useFactory: () => {  return new SocialsServiceDummy() }},
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    comp = fixture.componentInstance;

    fixture.detectChanges();
    });

    it('should test if all values are proper on start', () => {
      expect(comp.chooseSocialStream).toBeFalsy();
      expect(comp.activeStreams.length).toBe(0);
    })

    it('should open and close social chooser by clicking plus trigger without choosing', () => {
      spyOn(comp, 'openSocialStreamPopup').and.callThrough();
      let plusTrigger = fixture.debugElement.query(By.css('#plusTrigger')).nativeElement;
      click(plusTrigger);
      fixture.detectChanges();
      expect(comp.openSocialStreamPopup).toHaveBeenCalled();
      expect(comp.chooseSocialStream).toBeTruthy();

    })

    it('should close chooser by clicking outside', () => {
      spyOn(comp, 'closeSocialStreamPopup').and.callThrough();
      let plusTrigger = fixture.debugElement.query(By.css('#plusTrigger')).nativeElement;
      click(plusTrigger);
      fixture.detectChanges();

      comp.closeSocialStreamPopup();

      fixture.detectChanges();
      expect(comp.closeSocialStreamPopup).toHaveBeenCalled();
      expect(comp.chooseSocialStream).toBeFalsy();
    })


    it('should add selected accout to active streams', () =>  {
      localStorage.removeItem('activeSocialStreams');
      expect(comp.activeStreams.length).toBe(0);
      comp.onSelectedAccount(stubsAccount[0]);
      expect(comp.activeStreams.length).toBe(1);
      expect(comp.activeStreams[0]).toEqual(stubsAccount[0]);

      comp.onSelectedAccount(stubsAccount[1]);
      expect(comp.activeStreams.length).toBe(2);
      expect(comp.activeStreams[1]).toEqual(stubsAccount[1]);
    });

    it('should add selected accout to active streams', () =>  {
      spyOn(comp, 'removeFromActiveStream').and.callThrough();
      spyOn(comp, 'pushToSocialStreamChooserUsedSocials');

      comp.onSelectedAccount(stubsAccount[0]);
      comp.onSelectedAccount(stubsAccount[1]);

      expect(comp.activeStreams.length).toBe(4);

      comp.closeSocialTab(stubsAccount[0]);

      expect(comp.removeFromActiveStream).toHaveBeenCalledWith(stubsAccount[0]);
      expect(comp.pushToSocialStreamChooserUsedSocials).toHaveBeenCalledWith(stubsAccount[0]);
      expect(comp.activeStreams.length).toBe(2);
      expect(comp.activeStreams[0]).toEqual(stubsAccount[1]);
    });






});
