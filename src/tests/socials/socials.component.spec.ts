import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder,  FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ErrorDouble } from './../test-helpers/errorDouble';
import { SocialsServiceDummy } from './../test-helpers/socialsServiceDummy';
import { click } from './../test-helpers/clickHelper';



import { ErrorsService } from './../../app/services/errors.service';
import { SocialsService } from './../../app/services/socials.service';
import { SocialsComponent } from './../../app/socials/socials.component';
import { SocialEditComponent } from './../../app/socials/social-edit/social-edit.component';
import { AccordionModule } from 'ng2-bootstrap';
import { ModalModule } from 'ng2-bootstrap';

describe('Socials Component test', () => {
  class RouterStub {
    navigate(url: string) { return url; }
  };

  let fixture: ComponentFixture<SocialsComponent>;
  let comp:  SocialsComponent;
  let socialServiceSpy: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, AccordionModule.forRoot(), ModalModule.forRoot()],
      declarations: [ SocialsComponent, SocialEditComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: SocialsService, useFactory: () => { return new SocialsServiceDummy() } },
        { provide: Router, useFactory: () => { return new RouterStub() }},
        FormBuilder
      ],
    })
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(SocialsComponent);
    comp = fixture.componentInstance;

    socialServiceSpy = TestBed.get(SocialsService);
    spyOn(socialServiceSpy, 'getSocials').and.callThrough();
    spyOn(socialServiceSpy, 'addNewSocialAccount').and.callThrough();
    comp.ngOnInit();
    fixture.detectChanges();
  });


  it("should test if component is loading all values",  async(() => {
    expect(comp.accountCreated).toBeFalsy();
    expect(comp.socialsSites).toEqual(["facebook", "twitter", "linkedin"]);
    expect(comp.newSocialAccount.controls['social'].value).toBe('');
    expect(comp.newSocialAccount.controls['login'].value).toBe('');
    expect(comp.newSocialAccount.controls['password'].value).toBe('');
    expect(comp.newSocialAccount.controls['passwordConfirm'].value).toBe('');
    expect(socialServiceSpy.getSocials).toHaveBeenCalled();

    fixture.whenStable().then(() => {
    fixture.detectChanges();
     expect(comp.usedSocials.length).toBe(3);
     expect(comp.usedSocials[0]).toEqual({id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true});

     let accordionGrups = fixture.debugElement.queryAll(By.css("accordion-group"));
     expect(accordionGrups.length).toBe(3);

     let accordionGrupsSpans = fixture.debugElement.queryAll(By.css("accordion-group span"));
     expect(accordionGrupsSpans[0].nativeElement.innerText).toBe('TwitterUser');

     let accordionGrupsImgs = fixture.debugElement.queryAll(By.css("accordion-group img"));
     expect(accordionGrupsImgs[0].nativeElement.src).toContain("/assets/twitter.png")
     expect(accordionGrupsImgs[1].nativeElement.src).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg")

    });
  }));

  describe('form validation testing', ()=> {
    let socialInput: HTMLInputElement;
    let loginInput: HTMLInputElement;
    let passInput: HTMLInputElement;
    let passConfInput: HTMLInputElement;

    beforeEach(()=> {
      socialInput = fixture.debugElement.query(By.css('#social')).nativeElement as HTMLInputElement;
      loginInput = fixture.debugElement.query(By.css('#Login')).nativeElement as HTMLInputElement;
      passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      passConfInput = fixture.debugElement.query(By.css('#PasswordConfirm')).nativeElement as HTMLInputElement;
    })

    it("should validate social input", ()=> {
      expect(comp.newSocialAccount.get('social').hasError('required')).toBeTruthy();

      socialInput.value = "facebook";
      socialInput.checked = true;
      socialInput.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      expect(comp.newSocialAccount.get('social').hasError('required')).toBeFalsy();

    })

    it("should not validate password input due too short password", ()=> {
      passInput.value = "test";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.get('password').hasError('minlength')).toBeTruthy();
    })

    it("should validate password input", ()=> {
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.get('password').hasError('minlength')).toBeFalsy();
    })

    it("should not validate passwordConfirm input due too short password", ()=> {
      passConfInput.value = "test";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.get('passwordConfirm').hasError('minlength')).toBeTruthy();
    })

    it("should not validate passwordConfirm input", ()=> {
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.get('passwordConfirm').hasError('minlength')).toBeFalsy();
    })

    it("should not validate not equal passwords", ()=> {
      passInput.value = "test123456";
      passInput.dispatchEvent(new Event('input'));
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.hasError('areNotEquals')).toBeTruthy();
    })

    it("should validate equal passwords", ()=> {
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.newSocialAccount.hasError('areNotEquals')).toBeFalsy();
    })
  });


  describe('submitting form', ()=> {
    let socialInput: HTMLInputElement;
    let loginInput: HTMLInputElement;
    let passInput: HTMLInputElement;
    let passConfInput: HTMLInputElement;
    let form: HTMLElement;

    beforeEach(()=> {
      socialInput = fixture.debugElement.query(By.css('#social')).nativeElement as HTMLInputElement;
      socialInput.value = "facebook";
      socialInput.checked = true;
      socialInput.dispatchEvent(new Event('change'));

      passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));

      passConfInput = fixture.debugElement.query(By.css('#PasswordConfirm')).nativeElement as HTMLInputElement;
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));

      form = fixture.debugElement.query(By.css('form')).nativeElement;
      loginInput = fixture.debugElement.query(By.css('#Login')).nativeElement as HTMLInputElement;

      fixture.detectChanges();
    })

      it("should submit form, add new account and reset form by click", async(()=> {
        spyOn(comp, 'resetNewAccountForm').and.callThrough();
        loginInput.value = "unusedLogin";
        loginInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();


        fixture.whenStable().then(() => {
          fixture.detectChanges();

          form.dispatchEvent(new Event('submit'));
          fixture.detectChanges();
          fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(socialServiceSpy.addNewSocialAccount).toHaveBeenCalledWith({social: "facebook", login: "unusedLogin", password: "test123" });
            expect(comp.accountCreated).toBeTruthy();
            expect(comp.usedSocials.length).toBe(4);
            expect(comp.usedSocials[3]).toEqual({id: 7, social: "facebook", userPic: "pic.jpg", login: "unusedLogin", active: true});

            let resetButton = fixture.debugElement.query(By.css('.resetFormButton'));
            click(resetButton);
            fixture.detectChanges();

            expect(comp.resetNewAccountForm).toHaveBeenCalled();

            expect(comp.accountCreated).toBeFalsy();
            expect(comp.newSocialAccount.controls['social'].value).toBe('');
            expect(comp.newSocialAccount.controls['login'].value).toBe('');
            expect(comp.newSocialAccount.controls['password'].value).toBe('');
            expect(comp.newSocialAccount.controls['passwordConfirm'].value).toBe('');
          });


        });
      }));

      it("shouldn't add account because it already exists", async(()=> {
        spyOn(comp.accountAlreadyTaken, "show");

        loginInput.value = "FacebookUser";
        loginInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        fixture.whenStable().then(() => {
          fixture.detectChanges();

          form.dispatchEvent(new Event('submit'));
          fixture.detectChanges();

          expect(comp.accountAlreadyTaken.show).toHaveBeenCalled();
          expect(comp.accountCreated).toBeFalsy();
         });
      }));

  });

  describe('actions on accordion', ()=> {

      it("deleteAccount method", async(()=> {
        spyOn(socialServiceSpy, "deleteAccount").and.callThrough();

        fixture.whenStable().then(() => {
          fixture.detectChanges();
            comp.deleteAccount(comp.usedSocials[0]);
            fixture.detectChanges();
            fixture.whenStable().then(() => {
              fixture.detectChanges();
              expect(socialServiceSpy.deleteAccount).toHaveBeenCalledWith(1);
              expect(comp.usedSocials.length).toBe(2);
            });
          });
      }));




  });
});
