import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement,SimpleChange }    from '@angular/core';
import { FormBuilder,  FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ErrorDouble } from './../test-helpers/errorDouble';
import { SocialsServiceDummy } from './../test-helpers/socialsServiceDummy';
import { click } from './../test-helpers/clickHelper';

import { SocialAccount} from './../../app/shared/models/social-account';
import { ErrorsService } from './../../app/services/errors.service';
import { SocialsService } from './../../app/services/socials.service';
import { SocialEditComponent } from './../../app/socials/social-edit/social-edit.component';

describe('Socials-Edit Component test', () => {

  let fixture: ComponentFixture<SocialEditComponent>;
  let comp:  SocialEditComponent;
  let socialServiceSpy: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [SocialEditComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: SocialsService, useFactory: () => { return new SocialsServiceDummy() } },
        FormBuilder
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialEditComponent);
    comp = fixture.componentInstance;
    socialServiceSpy = TestBed.get(SocialsService);
    comp.account =  {id: 1, social: "twitter", userPic: "photo.jpg", login: "TwitterUser", active: true};
    comp.ngOnChanges(comp.account);
    fixture.detectChanges();
  });

  it("should check if component is loading all values",  () => {
    expect(comp.accountEdited).toBeFalsy();
    expect(comp.updatedSocialAccount.controls['id'].value).toBe(1);
    expect(comp.updatedSocialAccount.controls['login'].value).toBe('TwitterUser');
    expect(comp.updatedSocialAccount.controls['password'].value).toBe('');
  });

  describe('Socials-Edit validators test', () => {

    let loginInput: HTMLInputElement;
    let passInput: HTMLInputElement;
    let passConfInput: HTMLInputElement;

    beforeEach(()=> {
      loginInput = fixture.debugElement.query(By.css('#Login')).nativeElement as HTMLInputElement;
      passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      passConfInput = fixture.debugElement.query(By.css('#PasswordConfirm')).nativeElement as HTMLInputElement;
    })
    it("should not validate password input due too short password", ()=> {
      passInput.value = "test";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.get('password').hasError('minlength')).toBeTruthy();
    })

    it("should validate password input", ()=> {
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.get('password').hasError('minlength')).toBeFalsy();
    })

    it("should not validate passwordConfirm input due too short password", ()=> {
      passConfInput.value = "test";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.get('passwordConfirm').hasError('minlength')).toBeTruthy();
    })

    it("should not validate passwordConfirm input", ()=> {
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.get('passwordConfirm').hasError('minlength')).toBeFalsy();
    })

    it("should not validate not equal passwords", ()=> {
      passInput.value = "test123456";
      passInput.dispatchEvent(new Event('input'));
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.hasError('areNotEquals')).toBeTruthy();
    })

    it("should validate equal passwords", ()=> {
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));
      passConfInput.value = "test123";
      passConfInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.updatedSocialAccount.hasError('areNotEquals')).toBeFalsy();
    })
  });


  describe('submitting update form', ()=> {

    it("should submit form, update account and reset form by click", async(()=> {
        spyOn(socialServiceSpy, 'updateAccount').and.callThrough();
        spyOn(comp, 'resetEdition').and.callThrough();

        let passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
        passInput.value = "test123";
        passInput.dispatchEvent(new Event('input'));

        let passConfInput = fixture.debugElement.query(By.css('#PasswordConfirm')).nativeElement as HTMLInputElement;
        passConfInput.value = "test123";
        passConfInput.dispatchEvent(new Event('input'));

        let loginInput = fixture.debugElement.query(By.css('#Login')).nativeElement as HTMLInputElement;
        loginInput.value = "newLogin";
        loginInput.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        let form = fixture.debugElement.query(By.css('form')).nativeElement;
        form.dispatchEvent(new Event('submit'));
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          expect(socialServiceSpy.updateAccount).toHaveBeenCalledWith({id:1, login: "newLogin", password: "test123"});
          expect(comp.accountEdited).toBeTruthy();

          let resetButton = fixture.debugElement.query(By.css('button')).nativeElement
          click(resetButton);
          fixture.detectChanges();

          expect(comp.resetEdition).toHaveBeenCalled();
          expect(comp.accountEdited).toBeFalsy();
        });


    }));
  });
});
