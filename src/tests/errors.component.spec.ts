import { ComponentFixture, TestBed} from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ErrorsComponent } from  './../app/errors.component';
import { ErrorsService } from './../app/services/errors.service';
import { ErrorDouble } from './test-helpers/errorDouble';


describe('Error Component test', () => {
  let fixture: ComponentFixture<ErrorsComponent>;
  let comp:  ErrorsComponent;
  let errorsService: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsComponent ],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
      ],
    })

    fixture = TestBed.createComponent(ErrorsComponent);
    comp = fixture.componentInstance;
    errorsService = TestBed.get(ErrorsService);
    fixture.detectChanges();
  });

  it('should display error msg', ()=> {
    expect(comp.errorMessage).toBeUndefined();

    errorsService.triggerErrorMsg();
    fixture.detectChanges();

    expect(comp.errorMessage).toBe('An error occured');

    let errorSpan = fixture.debugElement.query(By.css('span')).nativeElement;
    expect(errorSpan.innerText).toBe('An error occured');
  });


  it('should disable error msg', ()=> {
    comp.disableErrorMsg();
    fixture.detectChanges();
    expect(comp.errorMessage).toBe('');

  });

});
