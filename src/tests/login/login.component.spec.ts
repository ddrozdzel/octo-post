import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { Router} from '@angular/router';
import { Observable } from 'rxjs';
import "rxjs/add/observable/of";
import { FormBuilder,  FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ErrorDouble } from './../test-helpers/errorDouble';
import { AuthLoginDouble } from  './../test-helpers/authLoginDouble';

import { ErrorsService } from './../../app/services/errors.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

import { LoginComponent } from './../../app/login/login.component';



describe('Login Component test', () => {
  class RouterStub {
    navigate(url: string) { return url; }
  };

  let fixture: ComponentFixture<LoginComponent>;
  let comp:  LoginComponent;
  let authLoginSpy: any;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ LoginComponent ],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: AuthLoginService, useFactory: () => { return new AuthLoginDouble() } },
        { provide: Router, useFactory: () => { return new RouterStub() }},
        FormBuilder
      ],
    })
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    comp = fixture.componentInstance;

    authLoginSpy = TestBed.get(AuthLoginService);
    spyOn(authLoginSpy, 'logout');
    comp.ngOnInit();
    fixture.detectChanges();
  });

  describe('form testing', () => {

    it('should have all attributes set propely at begining', () => {

       expect(authLoginSpy.logout).toHaveBeenCalled();
       expect(comp.wrongLoginPass).toBeFalsy();
       expect(comp.user.controls['login'].value).toBe('');
       expect(comp.user.controls['password'].value).toBe('');
      }
    );

    it('should check if password is not validated ', () => {
      let passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      passInput.value = "maly";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.user.get('password').hasError('minlength')).toBeTruthy();
      }
    );

    it('should check if password is validated ', () => {
      let passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      passInput.value = "WystarczajacoDlugi";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(comp.user.get('password').hasError('minlength')).toBeFalsy();
      }
    );

  });

  describe('login testing', () => {
    let loginInput: HTMLInputElement;
    let passInput: HTMLInputElement;
    let form: HTMLElement;
    let routerSpy: any;

    beforeEach(()=> {
      routerSpy = TestBed.get(Router);
      loginInput = fixture.debugElement.query(By.css('#Login')).nativeElement as HTMLInputElement;
      passInput = fixture.debugElement.query(By.css('#Password')).nativeElement as HTMLInputElement;
      form = fixture.debugElement.query(By.css('form')).nativeElement;
    })

    it('should submit form',  () => {
      spyOn(authLoginSpy, 'login').and.returnValue(Observable.of(true));
      spyOn(routerSpy, 'navigate');

      expect(comp.user.invalid).toBeTruthy();

      loginInput.value = "test";
      loginInput.dispatchEvent(new Event('input'));
      passInput.value = "test123";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      form.dispatchEvent(new Event('submit'));
      fixture.detectChanges();

      expect(authLoginSpy.login).toHaveBeenCalledWith("test", "test123");
      expect(comp.wrongLoginPass).toBeFalsy('credentials confirmed');
      expect(routerSpy.navigate).toHaveBeenCalledWith(['/dashboard']);
       }
    );

    it('should not submit form due to bad credentials', async(() => {
      spyOn(authLoginSpy, 'login').and.returnValue(Observable.of(false));
      expect(comp.user.invalid).toBeTruthy();

      loginInput.value = "badLogin";
      loginInput.dispatchEvent(new Event('input'));
      passInput.value = "badPassword";
      passInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      form.dispatchEvent(new Event('submit'));
      fixture.detectChanges();

      expect(comp.user.invalid).toBeFalsy();
      expect(authLoginSpy.login).toHaveBeenCalledWith("badLogin", "badPassword");
      expect(comp.wrongLoginPass).toBeTruthy();
    }));
  });

});
