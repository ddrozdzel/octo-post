import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { AuthGuardService } from './../../app/services/auth-guard.service';

describe('Auth Guard Service test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [AuthGuardService],
        imports: [RouterTestingModule]
      });
  });


  it('for not logged redirect to /login', async(inject([AuthGuardService, Router], (auth, router) => {
      spyOn(router, 'navigate');
      expect(auth.canActivate()).toBeFalsy();
      expect(router.navigate).toHaveBeenCalledWith(['/login']);
    })
  ));

  it('check if user is logged', async(inject([AuthGuardService, Router], (auth, router) => {
      let store = { currentUser: 'UserLogged'};

      spyOn(localStorage, 'getItem').and.callFake( (key:string):String => {
       return store[key];
      });

      expect(auth.canActivate()).toBeTruthy();
    })
  ));


});
