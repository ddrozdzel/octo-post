import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';

import { FacebookPostShareService } from './../../app/services/facebook-post-share.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

//import { FacebookPostShare  } from './../../app/shared/facebookEvent';




describe('FacebookPostShareService test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};

  let fbPostShareStub = {
    postId: 3, account: 4, comment: "ShareThis", privacy: "private"
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
        FacebookPostShareService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should share a post using shareFacebookPost',
    async(inject([FacebookPostShareService, MockBackend, AuthLoginService], (facebookPostShareService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createResponse();

      facebookPostShareService.shareFacebookPost(fbPostShareStub).then(
        response=> {
          expect(response).toBeNull();
        }
      );
    })
  ));



  it('should expect error from shareFacebookPost ',
    async(inject([FacebookPostShareService, MockBackend, AuthLoginService], (facebookPostShareService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createError();

      facebookPostShareService.shareFacebookPost(fbPostShareStub).then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );
    })
  ));




});
