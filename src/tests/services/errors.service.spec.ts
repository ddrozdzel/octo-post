import { TestBed, async, inject } from '@angular/core/testing';

import { ErrorsService } from './../../app/services/errors.service';
import { LoggerService } from './../../app/services/logger.service';
import { Response } from '@angular/http';

describe('Errors Service Test', () => {
    let errorsService : ErrorsService;
    const loggerServiceStub = {logError(error: any)  {}};
    const errorStub  = {status: 404, statusText: "errorOccured"};

  beforeEach(() => {
    errorsService = new ErrorsService(loggerServiceStub as LoggerService);
  });

  it('should check if is no error', ()=> {
    expect(errorsService.getError).toBeFalsy();
  })

  it('should start ErrorAction and create error message', () => {
    errorsService.startErrorAction(errorStub as Response);

    expect(errorsService.getError).toBe(errorStub);
    expect(errorsService.getErrorMessage).toBe(404 + "\n" + 'errorOccured');
  });



  it('should check if sendError/logError was called', () => {
    spyOn(errorsService, 'sendErrorSignal');
    spyOn(loggerServiceStub, 'logError');

    errorsService.startErrorAction(errorStub as Response);

    expect(loggerServiceStub.logError).toHaveBeenCalledWith(errorStub);
    expect(errorsService.sendErrorSignal).toHaveBeenCalledWith(404 + "\n" + 'errorOccured');
      //TODO: observable length
  //  expect(errorsService.errorSingalSource.observers.length).toBe(1)
  });


});
