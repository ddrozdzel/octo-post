import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';

import { FacebookEventsService } from './../../app/services/facebook-events.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

import { FacebookEvent  } from './../../app/shared/models/facebook-event';




describe('Facebook Events test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};

  let eventsStub = {
    data:
      [
        {id: 1, event:"Koncert Łąki Łan", owner: 'Łąki Łan', date: '06-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/255962424790662', status: 'Attend'},
        {id: 2, event:"Rap Festival", owner: 'Spodek Katowice', date: '16-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/1025874204192480', status: 'Respond'}
      ]
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
        FacebookEventsService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should get events from getFacebookEvents',
    async(inject([FacebookEventsService, MockBackend, AuthLoginService], (facebookEventsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, eventsStub);
      mockApi.createResponse();

      facebookEventsService.getFacebookEvents().then(
        response=> {
          expect(response.length).toBe(2);
          expect(response[0].id).toBe(1);
          expect(response[0].event).toBe("Koncert Łąki Łan");
          expect(response[0].owner).toBe("Łąki Łan");
          expect(response[0].date).toBe("06-12-2016 16:00");
          expect(response[0].respondUrl).toBe("https://www.facebook.com/events/255962424790662");
          expect(response[0].status).toBe("Attend");
        }
      );
    })
  ));



  it('should expect error from getFacebookEvents ',
    async(inject([FacebookEventsService, MockBackend, AuthLoginService], (facebookEventsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createError();

      facebookEventsService.getFacebookEvents().then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );
    })
  ));




});
