import { Http, Headers } from '@angular/http';

import { LoggerService } from './../../app/services/logger.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

describe('Logger Service test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub  = {status: 404, statusText: "errorOccured"};
  const httpStub = { post(url: string, body:any, headers: Headers){}};
  let loggerService: LoggerService;

  beforeEach(() => {
    loggerService = new LoggerService(httpStub as Http, authLoginStub as AuthLoginService);
  });

  it('should call log error', () => {
    spyOn(loggerService, 'logError');
  //  spyOn(httpStub, 'post');

    loggerService.logError(errorStub);

    //expect(httpStub.post).toHaveBeenCalled();
    expect(loggerService.logError).toHaveBeenCalledWith(errorStub);
  });
});
