import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';
import { AuthLoginService } from './../../app/services/auth-login.service';


describe('Auth Login Service test', () => {
  let loginDataStub = {login: 'test', password: 'test123'};
  let currentUserStub = {"login":"test",'token': '123456'};

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          AuthLoginService,
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });

      spyOn(localStorage, 'getItem').and.callFake( () => {
       return JSON.stringify(currentUserStub);
      });

  });


  it('should get token from local storage',
    async(inject([AuthLoginService, MockBackend], (authLoginService, mockBackend) => {
      expect(authLoginService.token).toBe('123456');
    }))
  );

  it('should login user and set localStorage',
    async(inject([AuthLoginService, MockBackend], (authLoginService, mockBackend) => {
        let mockApiConnect = new MockApiConnect(mockBackend, currentUserStub);
        mockApiConnect.createResponse();

        expect(authLoginService.login(loginDataStub.login, loginDataStub.password)).toBeTruthy();
        expect(authLoginService.token).toBe('123456');
    }))
  );

  it('should logout user',
    async(inject([AuthLoginService, MockBackend], (authLoginService, mockBackend) => {
        authLoginService.logout()
        expect(authLoginService.token).toBeNull();
    }))
  );


});
