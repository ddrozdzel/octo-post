import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect} from './../test-helpers/mock-api-connect';

import { FacebookPostsService } from './../../app/services/facebook-posts.service';
import { AuthLoginService } from './../../app/services/auth-login.service';


describe('FacebookPosts service', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};
  const idInfoReplieStub = {postId: 1, anwserId: 3};
  let fbPostsStub = {
    data: [
    {
       id: 1,
       date : "3 days ago",
       msg: "Pierwszy wpis na fejse",
       likes: 4,
       shares: 1,
       anwsers: [
         {
           id: 1,
           date: "10 minutes ago",
           user: "Janusz",
           userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
           msg: "Co jest?",
           likes: 2,
           replies: false
         },
         {
           id: 2,
           date: "12 minutes ago",
           user: "Janusz 2",
           userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
           msg: "Elo, elo",
           likes: 1,
           replies: true
         }
       ]
     },
     {
       id: 2,
       date: "3 hours ago",
       msg: "Drugi wpis - ty kupo",
       likes: 0,
       shares: 2,
       anwsers: []
     }
   ]
  };

  let fbRepliesStub = {
    data: [
            {
              id: 1,
              date: "10 minutes ago",
              user: "Bożena",
              userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
              msg: "O ty pierunie, wylaz z neta?",
              likes: 2,
            },
            {
              id: 2,
              date: "12 minutes ago",
              user: "Janusz",
              userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
              msg: "Ale bożena, jak to?",
              likes: 0,
            }
        ]
    };




  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          FacebookPostsService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should get Facebook Posts from api using getFacebookPosts',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, fbPostsStub);
      mockApi.createResponse();
      facebookPostsService.getFacebookPosts().then(
        response=> {
          expect(response.length).toBe(2);
          expect(response[0].id).toBe(1);
          expect(response[0].date).toBe("3 days ago");
          expect(response[0].msg).toBe("Pierwszy wpis na fejse");
          expect(response[0].likes).toBe(4);
          expect(response[0].shares).toBe(1);

          expect(response[0].anwsers.length).toBe(2);
          expect(response[0].anwsers[0].id).toBe(1);
          expect(response[0].anwsers[0].date).toBe('10 minutes ago');
          expect(response[0].anwsers[0].user).toBe('Janusz');
          expect(response[0].anwsers[0].userPic).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
          expect(response[0].anwsers[0].msg).toBe("Co jest?");
          expect(response[0].anwsers[0].likes).toBe(2);
          expect(response[0].anwsers[0].replies).toBeFalsy();
        }
      );
    })
  ));

  it('should get Facebook Replies from api using getReplies',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, fbRepliesStub);
      mockApi.createResponse();
      facebookPostsService.getReplies(1,2).then(
        response=> {
          expect(response.length).toBe(2);
          expect(response[0].id).toBe(1);
          expect(response[0].date).toBe("10 minutes ago");
          expect(response[0].user).toBe("Bożena");
          expect(response[0].userPic).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
          expect(response[0].msg).toBe("O ty pierunie, wylaz z neta?");
          expect(response[0].likes).toBe(2);

        }
      );
    })
  ));


  it('should add replie to anwser using addReplie',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, {data:[fbRepliesStub.data[0]]});
      mockApi.createResponse();

      facebookPostsService.addReplie(idInfoReplieStub, 'Some replie').then(
        response=> {
          expect(response[0].id).toBe(1);
          expect(response[0].date).toBe("10 minutes ago");
          expect(response[0].user).toBe("Bożena");
          expect(response[0].userPic).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
          expect(response[0].msg).toBe("O ty pierunie, wylaz z neta?");
          expect(response[0].likes).toBe(2);

        }
      );
    })
  ));


  it('should add comment to post using addComment',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, {data:[fbPostsStub.data[0].anwsers[0]]});
      mockApi.createResponse();

      facebookPostsService.addComment(1, 'Some comment').then(
        response=> {
          expect(response[0].id).toBe(1);
          expect(response[0].date).toBe('10 minutes ago');
          expect(response[0].user).toBe('Janusz');
          expect(response[0].userPic).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
          expect(response[0].msg).toBe("Co jest?");
          expect(response[0].likes).toBe(2);
          expect(response[0].replies).toBeFalsy();

        }
      );
    })
  ));


  it('should add Like to post using addLike',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, true);
      mockApi.createResponse();

      facebookPostsService.addLike(1, 2,3).then(
        response=> {
          expect(response).toBeTruthy();
        }
      );
    })
  ));


  it('should expect error from all FacebookPostService method ',
    async(inject([FacebookPostsService, MockBackend, AuthLoginService], (facebookPostsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createError();

      facebookPostsService.getFacebookPosts().then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      facebookPostsService.getReplies(1,2).then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      facebookPostsService.addReplie(idInfoReplieStub, 'Some replie').then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      facebookPostsService.addComment(3, 'comment').then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      facebookPostsService.addLike(3,null, null).then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );


    })
  ));
});
