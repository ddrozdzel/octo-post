import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';

import { FacebookAddPostService } from './../../app/services/facebook-add-post.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

import { FacebookNewPost } from './../../app/shared/models/facebook-new-post';




describe('Facebook Add Post service test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};
  const newFbPostStub: FacebookNewPost = {msg: 'test', link: 'link', privacy: 'private',   attachments: [], schedule: ''};

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          FacebookAddPostService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should add new Facebook Post to Api',
    async(inject([FacebookAddPostService, MockBackend, AuthLoginService], (facebookAddPostService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createResponse();
      facebookAddPostService.addFacebookPost(newFbPostStub).then(
        response=> {
          expect(response).toBeNull();
        }
      );
    })
  ));

  it('should expect error from addPost',
    async(inject([FacebookAddPostService, MockBackend, AuthLoginService], (facebookAddPostService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createError();
      facebookAddPostService.addFacebookPost(newFbPostStub).then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );
    })
  ));




});
