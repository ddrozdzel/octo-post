import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';

import { SocialsService } from './../../app/services/socials.service';
import { AuthLoginService } from './../../app/services/auth-login.service';
import { NewSocialAccount } from './../../app/shared/models/new-social-account';



describe('SocialService test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};
  const newSocialAccountStub: NewSocialAccount = {social: 'twitter', login: 'user', password: 'password'};
  const addedSocialAccountStub = {data: {id: 7, social: "twitter", userPic: "pic.jpg", login: "user", active: true}};
  const socialsStub = {
     data: [
      {id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true},
      {id: 5, social: "linkedin", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "LinkedinUser", active: true},
      {id: 6, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser", active: true}
    ]
  };



  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          SocialsService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should get socials from backend using getSocials() method',
    async(inject([SocialsService, MockBackend, AuthLoginService], (socialsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, socialsStub);
      mockApi.createResponse();

      socialsService.getSocials().then(
        response => {
          expect(response.length).toBe(3);
          expect(response[0].id).toBe(1);
          expect(response[0].login).toBe("TwitterUser");
          expect(response[0].social).toBe("twitter");
          expect(response[0].userPic).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
          expect(response[0].active).toBeTruthy();

        }
      );
    })
  ));

  it('should get add new social account using addNewSocialAccount method',
    async(inject([SocialsService, MockBackend, AuthLoginService], (socialsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, addedSocialAccountStub);
      mockApi.createResponse();

      socialsService.addNewSocialAccount(newSocialAccountStub).then(
        response => {
          expect(response.id).toBe(7);
          expect(response.social).toBe("twitter");
          expect(response.login).toBe("user");
          expect(response.userPic).toBe("pic.jpg");
          expect(response.active).toBeTruthy();
        }
      );
    })
  ));

  it('should delete social account using deleteAccount method',
    async(inject([SocialsService, MockBackend, AuthLoginService], (socialsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createResponse();

      socialsService.deleteAccount(1).then(
        response => {
          expect(response).toBeNull();
        }
      );
    })
  ));

  it('should update social account using updateAccount method',
    async(inject([SocialsService, MockBackend, AuthLoginService], (socialsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createResponse();

      socialsService.updateAccount(addedSocialAccountStub.data).then(
        response => {
          expect(response).toBeNull();
        }
      );
    })
  ));

  it('should get expected errors from backend methods',
    async(inject([SocialsService, MockBackend, AuthLoginService], (socialsService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, errorStub);
      mockApi.createResponse();

      socialsService.getSocials().then(
        response => {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      socialsService.addNewSocialAccount(newSocialAccountStub).then(
        response => {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      socialsService.deleteAccount(1).then(
        response => {},
        error => {
          expect(error.status).toBe(404);
        }
      );

      socialsService.updateAccount(addedSocialAccountStub.data).then(
        response => {},
        error => {
          expect(error.status).toBe(404);
        }
      );

    })
  ));

});
