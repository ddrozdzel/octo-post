import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import {MockApiConnect } from './../test-helpers/mock-api-connect';

import { FacebookSharesService } from './../../app/services/facebook-shares.service';
import { AuthLoginService } from './../../app/services/auth-login.service';

import { FacebookInteractUsers } from './../../app/shared/models/facebook-interact-users';




describe('FacebookSharesService test', () => {
  const authLoginStub  = {token: "123456789"};
  const errorStub = {status: 404, statusText: "errorOccured"};
  let fbSharesStub = {
    data:
      [
        {
          user: "Jasiek",
          userPic: "https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
        },
        {
          user: "Marek",
          userPic: "httdb3949e4"
        }
       ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [
          FacebookSharesService,
          { provide: AuthLoginService, useValue:authLoginStub },
          {
            provide: Http,
            useFactory: (mockBackend, options) => {
              return new Http(mockBackend, options)
            },
            deps: [MockBackend, BaseRequestOptions]
          },
          MockBackend,
          BaseRequestOptions
        ],
        imports: [HttpModule]
      });
  });

  it('should get facebook shares from api using getFacebookShares',
    async(inject([FacebookSharesService, MockBackend, AuthLoginService], (facebookSharesService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, fbSharesStub);
      mockApi.createResponse();
      facebookSharesService.getFacebookShares(1).then(
        response=> {
          expect(response.length).toBe(2);
          expect(response[0].user).toBe('Jasiek');
          expect(response[0].userPic).toBe("https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg");
        }
      );
    })
  ));

  it('should expect error from getFacebookShares',
    async(inject([FacebookSharesService, MockBackend, AuthLoginService], (facebookSharesService, mockBackend, authLoginStub) => {
      let mockApi = new MockApiConnect(mockBackend, null);
      mockApi.createError();
      facebookSharesService.getFacebookShares(1).then(
        response=> {},
        error => {
          expect(error.status).toBe(404);
        }
      );
    })
  ));

});
