import { SocialLoginSearchPipe } from './../../app/shared/social-login-search.pipe';

describe('Social Login Search Pipe test', ()=> {
  let pipe: SocialLoginSearchPipe;
  let socials: any[];

  beforeEach(() => {
    pipe = new SocialLoginSearchPipe();
    socials =  [
      {id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true},
      {id: 5, social: "linkedin", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "LinkedinUser", active: true},
      {id: 6, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser", active: true}
      ];
  });

  it('should filter FacebookAccount account from available accounts', ()=> {
    let transformed =  pipe.transform(socials, 'Facebook');;
    expect(transformed.length).toBe(1);
    expect(transformed).toEqual([{id: 6, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser", active: true}])
  });

  it('should filter accounts starts with "Tw" from available accounts', ()=> {
    let transformed = pipe.transform(socials, 'Tw');
    expect(transformed.length).toBe(1);
    expect(transformed).toEqual([{id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true}])
  });


  it('should filter accounts starts with "DDD" from available accounts and show nothing', ()=> {
    let transformed =pipe.transform(socials, 'DDD');
    expect(transformed.length).toBe(0);
  });

  it('should not filter from available accounts and show all', ()=> {
    let transformed = pipe.transform(socials, '');
    expect(transformed.length).toBe(3);
    expect(transformed).toEqual(socials);
  });

});
