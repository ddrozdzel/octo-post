import { SafeUrlPipe} from './../../app/shared/safe-url.pipe';
import { DomSanitizer } from '@angular/platform-browser';
import { TestBed, inject, async } from '@angular/core/testing';

describe('SafeUrlPipe', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [DomSanitizer]
    });
  });

  it('create an instance', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
    const pipe = new SafeUrlPipe(sanitizer);
    expect(pipe).toBeTruthy();
  }));

  // it('valid an link with pipe', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
  //   const pipe = new SafeUrlPipe(sanitizer);
  //   let linkTest = "<a href='http://wp.pl'>wp</a>";
  //   let transformed =  pipe.transform(linkTest);
  //   expect(transformed).toBe(linkTest);
  // }));

});
