import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { SocialsServiceDummy } from './../test-helpers/socialsServiceDummy';
import { ErrorDouble } from './../test-helpers/errorDouble';
import { ErrorsService } from './../../app/services/errors.service';
import { click } from './../test-helpers/clickHelper';


import { SocialsService } from './../../app/services/socials.service';
import { SocialStreamChooserComponent } from './../../app/shared/components/social-stream-chooser/social-stream-chooser.component';
import { SocialLoginSearchPipe } from './../../app/shared/social-login-search.pipe';

describe('SocialStreamChooserComponent test', () => {
  let fixture: ComponentFixture<SocialStreamChooserComponent>;
  let comp:  SocialStreamChooserComponent;
  let errorDoubleSpy: any;
  let accountStub: any = {id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [  SocialStreamChooserComponent, SocialLoginSearchPipe],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: SocialsService, useFactory: () => {  return new SocialsServiceDummy() }}

      ],
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialStreamChooserComponent);
    comp = fixture.componentInstance;

    errorDoubleSpy = TestBed.get(ErrorsService);
    spyOn(errorDoubleSpy, 'startErrorAction');

    comp.ngOnInit();
    fixture.detectChanges();
  });


  it('should test socials from API using getSocials', async(() => {
    let logins = ['TwitterUser', 'LinkedinUser', 'FacebookUser' ];
    let loopId = 0;
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(comp.usedSocials.length).toBe(3);
      expect(comp.usedSocials[0]).toEqual(accountStub);

      let accountsSpan = fixture.debugElement.queryAll(By.css('span'));
      accountsSpan.forEach((debugEl) => {
        expect(debugEl.nativeElement.innerText).toBe(logins[loopId]);
        loopId++;
      })

      let imgs = fixture.debugElement.queryAll(By.css("img"));
      expect(imgs[0].nativeElement.src).toContain("/assets/twitter.png")
      expect(imgs[1].nativeElement.src).toBe("https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg")
    });
  }));

  it('should test selectAccount by choosing social account', async(() => {
    spyOn(comp.selectedAccount, 'emit');
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      let selectAccountButton = fixture.debugElement.queryAll(By.css("div[role='button']"));
      click(selectAccountButton[0]);
      fixture.detectChanges();
      expect(comp.selectedAccount.emit).toHaveBeenCalledWith(accountStub);
      expect(comp.usedSocials.length).toBe(2);
      expect(comp.usedSocials[0].social).toBe("linkedin");
    });
  }));

  it('should attach to usedSocials social account using addToUsedSocials', async(() => {
    let unusedAccountStub = {id: 4, social: "twitter", userPic: "test.jpg", login: "Auser", active: true};
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      comp.addToUsedSocials(unusedAccountStub);
      expect(comp.usedSocials.length).toBe(4);
      expect(comp.usedSocials[0].login).toBe('Auser');
      expect(comp.usedSocials[1].login).toBe('FacebookUser');
      expect(comp.usedSocials[2].login).toBe('LinkedinUser');
      expect(comp.usedSocials[3].login).toBe('TwitterUser');
    });
  }));

  it('should test socialSearchBox with facebookUser', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();

      let input= fixture.debugElement.query(By.css("input")).nativeElement as HTMLInputElement;
      input.value = "Link";
      input.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      let accountsSpan = fixture.debugElement.queryAll(By.css('span'));
      expect(accountsSpan[0].nativeElement.innerText).toBe('LinkedinUser');
    });
  }));




});
