import { isEmptyObject } from './../../app/shared/validators';
import { areEqual } from './../../app/shared/validators';
import { FormControl } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


interface Validator<T extends FormControl> {
   (c:T): {[error: string]:any};
}

describe('validators function tests ', () => {

  describe('function: isEmptyObject', () => {
      it('should be validated', () => {
        let formControl = new FormControl({id: 1},isEmptyObject);
        expect(formControl.status).toBe('VALID');
      });

      it('should not be validated (emptyobject)', () => {
        let formControl = new FormControl({},isEmptyObject);
        expect(formControl.status).toBe('INVALID');
      });

  });

  describe('function: areEqual', () => {
      it('should be validated (emptyobject)', () => {
        const formGroup = new FormGroup({
          password       : new FormControl('ABC'),
          passwordConfirm : new FormControl('ABC')
          }, areEqual('password', 'passwordConfirm')
        );
        expect(formGroup.status).toBe('VALID');
      });




      it('should not be validated (not Eqaul)', () => {
        const formGroup = new FormGroup({
          password       : new FormControl('ABC'),
          passwordConfirm : new FormControl('ABCD')
          }, areEqual('password', 'passwordConfirm')
        );
        expect(formGroup.status).toBe('INVALID');
      });

  });




});
