import { ComponentFixture, TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { SocialStreamChooserComponent } from './../../app/shared/components/social-stream-chooser/social-stream-chooser.component';
import { ErrorsService } from './../../app/services/errors.service';
import { SocialsService } from './../../app/services/socials.service';
import { FacebookAddPostService } from './../../app/services/facebook-add-post.service';
import { AccordionModule } from 'ng2-bootstrap';
import { DropzoneModule } from 'angular2-dropzone-wrapper';
import { DROPZONE_CONFIG } from './../../app/shared/dropzone-config';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SocialLoginSearchPipe } from './../../app/shared/social-login-search.pipe';
import {AccordionAccountComponent } from './../../app/shared/components/accordion-account/accordion-account.component';


import { FormGroup, FormBuilder, Validators,  FormControl } from '@angular/forms';
import { FacebookAddPostServiceDummy} from './../test-helpers/facebookAddPostServiceDummy';
import { SocialsServiceDummy } from './../test-helpers/socialsServiceDummy';
import { ErrorDouble } from './../test-helpers/errorDouble';
import { RouterLinkStubDirective } from './../test-helpers/routerLinkStubDirective';
import { click } from './../test-helpers/clickHelper';


import { AddPostComponent } from './../../app/addpost/addpost.component';

describe('AddPost Component test', () => {
  let fixture: ComponentFixture<AddPostComponent>;
  let comp:  AddPostComponent;
  let facebookAddPostServiceSpy: any;
  let errorDoubleSpy: any;

  const socialAccountStub = {id:1, social: "twitter", userPic: 'none', login: 'twitterUser', active: true };
  const socialAccountStub2 = {id:2, social: "facebook", userPic: 'none', login: 'facebookUser', active: true };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AccordionModule.forRoot(),  DropzoneModule.forRoot(DROPZONE_CONFIG),FormsModule,ReactiveFormsModule ],
      declarations: [ AddPostComponent, SocialStreamChooserComponent, RouterLinkStubDirective, SocialLoginSearchPipe, AccordionAccountComponent],
      providers: [
        { provide: ErrorsService, useFactory: () => { return new ErrorDouble() } },
        { provide: SocialsService, useFactory: () => {  return new SocialsServiceDummy() }},
        { provide: FacebookAddPostService, useFactory: () => {  return new FacebookAddPostServiceDummy() } },
        FormBuilder
      ],
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPostComponent);
    comp = fixture.componentInstance;
    facebookAddPostServiceSpy = TestBed.get(FacebookAddPostService);
    spyOn(facebookAddPostServiceSpy, 'addFacebookPost').and.callThrough();

    errorDoubleSpy = TestBed.get(ErrorsService);
    spyOn(errorDoubleSpy, 'startErrorAction');

    fixture.detectChanges();
  });


     it('should have all attributes set propely at begining', () => {
      expect(comp.maxMsglength).toBe(63206);
      expect(comp.newPostAddedSucces).toBeFalsy();
      expect(comp.privacies).toEqual(["Only Me", "Friends", "Friends of My Friends", "Public", "Other"]);
      expect(comp.informationSend).toBeFalsy();
      expect(Object.keys(comp.accountsStorage).length).toBe(0);
      expect(comp.attachmentUploaded).toBeFalsy();
      expect(comp.choosedAccounts.length).toBe(0);

      expect(comp.facebookNewPost.controls['msg'].value).toBe('');
      expect(comp.facebookNewPost.controls['link'].value).toBe('');
      expect(comp.facebookNewPost.controls['privacy'].value).toBe('');
      expect(comp.facebookNewPost.controls['schedule'].value).toBe('');
      expect(Object.keys(comp.facebookNewPost.controls['account'].value).length).toBe(0);
     }
   );


     it('should test if privaces rendered to addpost.component.html', () => {
      const options = fixture.debugElement.queryAll(By.css('option'));
      expect(options.length).toBe(5);
      expect(options[0].nativeElement.value).toBe("Only Me");


      // const social_stream_chooser =  fixture.debugElement.queryAll( de => de.references['socialChooser']);
      // console.log(social_stream_chooser[0].nativeNode);

     }
    );

    it('should test if input accounts empty values is proper validated', () => {
      let accountsInput = fixture.debugElement.query(By.css('#accounts')).nativeElement as HTMLInputElement;
      accountsInput.value = JSON.stringify({});
      expect(comp.facebookNewPost.get('account').hasError('isEmptyArray')).toBeTruthy();
      expect(comp.choosedAccounts.length).toBe(0);
      }
    );

    it('should test if input accounts values is proper validated', () => {
      comp.selectAccount(socialAccountStub);
      fixture.detectChanges();
      expect(comp.facebookNewPost.get('account').hasError('isEmptyArray')).toBeFalsy();
      let accordions = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(accordions.length).toBe(1);
      expect(accordions[0].nativeElement.innerText).toContain('twitterUser');

      comp.selectAccount(socialAccountStub2);
      fixture.detectChanges();
      expect(comp.facebookNewPost.get('account').hasError('isEmptyArray')).toBeFalsy();
      let accordions2 = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(accordions2.length).toBe(2);
      expect(accordions2[0].nativeElement.innerText).toContain('twitterUser');
      expect(accordions2[1].nativeElement.innerText).toContain('facebookUser');

      spyOn(comp, 'removeAccountFromChoosed').and.callThrough();
      spyOn(comp.socialChooser, 'addToUsedSocials');
      let removeButton = fixture.debugElement.queryAll(By.css('accordion-group button'));
      click(removeButton[0]);
      fixture.detectChanges();
      expect(comp.removeAccountFromChoosed).toHaveBeenCalledWith(socialAccountStub);
      let accordions3 = fixture.debugElement.queryAll(By.css('.account-login'));
      expect(accordions3.length).toBe(1);
      expect(accordions3[0].nativeElement.innerText).toContain('facebookUser');

      }
    );

    it('should test if input Message value is proper validated', () => {
       let messageInput = fixture.debugElement.query(By.css('#Message')).nativeElement as HTMLInputElement;
       messageInput.value = "Test wiadomości";
       messageInput.dispatchEvent(new Event('input'));
       fixture.detectChanges();
       expect(comp.facebookNewPost.get('msg').hasError('maxlength')).toBeFalsy();
      }
    );

    it('should test if input Message value is proper validated for twitter social', () => {
         let messageInput = fixture.debugElement.query(By.css('#Message')).nativeElement as HTMLInputElement;

         comp.selectAccount(socialAccountStub);

         messageInput.value = "Wiadomość większa niż 150 znaków. Wiadomość większa niż 150 znaków. Wiadomość większa niż 150 znaków. Wiadomość większa niż 150 znaków. Wiadomość większa niż 150 znaków.";
         messageInput.dispatchEvent(new Event('input'));
         fixture.detectChanges();
         expect(comp.facebookNewPost.get('msg').hasError('maxlength')).toBeTruthy();

         messageInput.value = "Test";
         messageInput.dispatchEvent(new Event('input'))
         fixture.detectChanges();
         expect(comp.facebookNewPost.get('msg').hasError('maxlength')).toBeFalsy();
      }
    );

    it('should test if input link value is proper validated', () => {
       let linkInput = fixture.debugElement.query(By.css('#link')).nativeElement as HTMLInputElement;
       linkInput.value = "http://gazeta.pl";
       linkInput.dispatchEvent(new Event('input'));
       fixture.detectChanges();
       expect(comp.facebookNewPost.get('link').hasError('pattern')).toBeFalsy();

      }
    );

    it('should test if wrong input link value is not validated', () => {
       let linkInput = fixture.debugElement.query(By.css('#link')).nativeElement as HTMLInputElement;
       let wrongUrls: string[] = ['www', 'ss', 'www.wp', 'ff fff'];
         wrongUrls.forEach((url) => {
           linkInput.value = url;
           linkInput.dispatchEvent(new Event('input'));
           fixture.detectChanges();
           expect(comp.facebookNewPost.get('link').hasError('pattern')).toBeTruthy();
        });
      }
    );

    it('should test if schedule input value is proper validated', () => {
       let scheduleInput = fixture.debugElement.query(By.css('#schedule')).nativeElement as HTMLInputElement;
       scheduleInput.value = "2017-02-01 12:23";
       scheduleInput.dispatchEvent(new Event('input'));
       fixture.detectChanges();
       expect(comp.facebookNewPost.get('schedule').hasError('pattern')).toBeFalsy();
      }
    );

    it('should test if wrong schedule input value is not validated', () => {
       let scheduleInput = fixture.debugElement.query(By.css('#schedule')).nativeElement as HTMLInputElement;
       let wrongSchedules: string[] = ['www', '11-09-2013 12:23:32', "2017-02-01 12:23:32", '13.02.2033 12:23', '13.02', '22.02.12 12:02:01', '2012.03.40 12:02.03'];
         wrongSchedules.forEach((url) => {
           scheduleInput.value = url;
           scheduleInput.dispatchEvent(new Event('input'));
           fixture.detectChanges();
           expect(comp.facebookNewPost.get('schedule').hasError('pattern')).toBeTruthy();
        });
      }
    );
    //https://github.com/angular/angular/blob/master/modules/%40angular/forms/test/template_integration_spec.ts
    it('should test if privacies input value is proper validated',() => {
       let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
       expect(comp.facebookNewPost.get('privacy').hasError('required')).toBeTruthy();
       privacyInput.value = "Only Me";
       privacyInput.checked = true;
       privacyInput.dispatchEvent(new Event('change'));
       fixture.detectChanges();
       expect(comp.facebookNewPost.get('privacy').hasError('required')).toBeFalsy();

      }
    );


    it('should test select/remove account in isolated test',() => {
        spyOn(comp, 'removeAccountFromChoosed').and.callThrough();
        spyOn(comp.socialChooser, 'addToUsedSocials');
        comp.selectAccount(socialAccountStub2);
        expect(comp.accountsStorage[2]).toBe('facebook');
        expect(comp.choosedAccounts[0]).toEqual(socialAccountStub2);
        expect(comp.maxMsglength).toBe(63206);

        comp.selectAccount(socialAccountStub);
        expect(comp.accountsStorage[1]).toBe('twitter');
        expect(comp.choosedAccounts[1]).toEqual(socialAccountStub);
        expect(comp.maxMsglength).toBe(150);

        comp.selectAccount(socialAccountStub);
        expect(comp.removeAccountFromChoosed).toHaveBeenCalledWith(socialAccountStub);
        expect(comp.choosedAccounts[1]).toBeUndefined();
        expect(comp.accountsStorage[1]).toBeUndefined();
        expect(comp.maxMsglength).toBe(63206);
        expect(comp.socialChooser.addToUsedSocials).toHaveBeenCalledWith(socialAccountStub);
      }
    );

    it('should test onUploadSuccess', () => {
      comp.onUploadSuccess();
      expect(comp.attachmentUploaded).toBeTruthy();
    })

    it("should test if form won't be submitted if one of three imputs won't be filled", () => {
      let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
      privacyInput.value = "Only Me";
      privacyInput.checked = true;
      privacyInput.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      let form = fixture.debugElement.query(By.css('form')).nativeElement;
      form.dispatchEvent(new Event('submit'));
      fixture.detectChanges();
      expect(comp.facebookNewPost.invalid).toBeTruthy();
      expect(comp.informationSend).toBeFalsy();
      expect(comp.newPostAddedSucces).toBeFalsy();
    }
  );

    it("should test if form is submitted if message input will be filled", async(() => {
      comp.selectAccount(socialAccountStub);

      let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
      privacyInput.value = "Only Me";
      privacyInput.checked = true;
      privacyInput.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      let messageInput = fixture.debugElement.query(By.css('#Message')).nativeElement as HTMLInputElement;
      messageInput.value = "Test wiadomości";
      messageInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      let form = fixture.debugElement.query(By.css('form')).nativeElement;
      form.dispatchEvent(new Event('submit'));
      fixture.detectChanges();
      fixture.whenStable().then(() => {
         fixture.detectChanges();
         expect(facebookAddPostServiceSpy.addFacebookPost).toHaveBeenCalled();
         expect(comp.informationSend).toBeTruthy();
         expect(comp.newPostAddedSucces).toBeTruthy();
       }
     );
    }
   ));

   it("should test if form is submitted if link input will be filled", async(() => {
     comp.selectAccount(socialAccountStub);

     let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
     privacyInput.value = "Only Me";
     privacyInput.checked = true;
     privacyInput.dispatchEvent(new Event('change'));
     fixture.detectChanges();

     let linkInput = fixture.debugElement.query(By.css('#link')).nativeElement as HTMLInputElement;
     linkInput.value = "http://gazeta.pl";
     linkInput.dispatchEvent(new Event('input'));
     fixture.detectChanges();

     let form = fixture.debugElement.query(By.css('form')).nativeElement;
     form.dispatchEvent(new Event('submit'));
     fixture.detectChanges();
     fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(facebookAddPostServiceSpy.addFacebookPost).toHaveBeenCalled();
        expect(comp.informationSend).toBeTruthy();
        expect(comp.newPostAddedSucces).toBeTruthy();
      }
    );
   }
  ));

    it("should test if form is submitted if attachmens will be submitet", async(() => {
      comp.selectAccount(socialAccountStub);

      let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
      privacyInput.value = "Only Me";
      privacyInput.checked = true;
      privacyInput.dispatchEvent(new Event('change'));
      fixture.detectChanges();

      comp.onUploadSuccess();
      fixture.detectChanges();

      let form = fixture.debugElement.query(By.css('form')).nativeElement;
      form.dispatchEvent(new Event('submit'));
      fixture.detectChanges();
      fixture.whenStable().then(() => {
         fixture.detectChanges();
         expect(facebookAddPostServiceSpy.addFacebookPost).toHaveBeenCalled();
         expect(comp.informationSend).toBeTruthy();
         expect(comp.newPostAddedSucces).toBeTruthy();
      });
     }
   ));

   it('should test onUploadError', () => {
     comp.onUploadError();
     expect(comp.attachmentUploaded).toBeFalsy();
     expect(errorDoubleSpy.startErrorAction).toHaveBeenCalled();
   })




   it("should test if form is submitted with all values and route do dashboard after click", async(() => {
     let submitDataStub =  {account: {1: 'twitter'}, msg: 'Test wiadomości', link: 'http://gazeta.pl', privacy: 'Only Me', schedule: '2017-02-01 12:23'};

     comp.selectAccount(socialAccountStub);

     let privacyInput = fixture.debugElement.query(By.css('#privacy')).nativeElement as HTMLInputElement;
     privacyInput.value = "Only Me";
     privacyInput.checked = true;
     privacyInput.dispatchEvent(new Event('change'));

     let linkInput = fixture.debugElement.query(By.css('#link')).nativeElement as HTMLInputElement;
     linkInput.value = "http://gazeta.pl";
     linkInput.dispatchEvent(new Event('input'));

     comp.onUploadSuccess();

     let messageInput = fixture.debugElement.query(By.css('#Message')).nativeElement as HTMLInputElement;
     messageInput.value = "Test wiadomości";
     messageInput.dispatchEvent(new Event('input'));

     privacyInput.value = "Only Me";
     privacyInput.checked = true;
     privacyInput.dispatchEvent(new Event('change'));

     let scheduleInput = fixture.debugElement.query(By.css('#schedule')).nativeElement as HTMLInputElement;
     scheduleInput.value = "2017-02-01 12:23";
     scheduleInput.dispatchEvent(new Event('input'));

     fixture.detectChanges();

     let form = fixture.debugElement.query(By.css('form')).nativeElement;
     form.dispatchEvent(new Event('submit'));
     fixture.detectChanges();
     fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(comp.facebookNewPost.value).toEqual(submitDataStub);
        expect(facebookAddPostServiceSpy.addFacebookPost).toHaveBeenCalled();
        expect(comp.informationSend).toBeTruthy();
        expect(comp.newPostAddedSucces).toBeTruthy();

        let linkDirectives = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective));
        let linktoDashboard = linkDirectives[0].injector.get(RouterLinkStubDirective) as RouterLinkStubDirective
        expect(linktoDashboard.navigatedTo).toBeNull('link should not have navigated yet');

        click(linkDirectives[0]);
        fixture.detectChanges();
        expect(linktoDashboard.navigatedTo).toContain('/dashboard');
     });
    }
  ));





});
