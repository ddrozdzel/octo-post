import { FacebookInteractUsers } from '../../app/shared/models/facebook-interact-users';
import { FacebookEvent } from './../../app/shared/models/facebook-event';
import { FacebookPostShare } from "./../../app/shared/models/facebook-post-share";
import { FacebookPostAnwser } from "./../../app/shared/models/facebook-post-anwser";
import { FacebookPost } from "./../../app/shared/models/facebook-post";

export class FbPostShareDummy {

    shareFacebookPost(sharePost: FacebookPostShare) : Promise<any> {
      return Promise.resolve(null);
    }
}


export class LikesSharesDummy {
    private interacts: FacebookInteractUsers[] = [
      {
        user: "Jasiek",
        userPic: "photo1.jpg"
      },
      {
        user: "Marek",
        userPic: "photo2.jpg"
      }
    ];

    getLikesShares() {
      return this.interacts;
    }

    getFacebookShares(id: number) : Promise<FacebookInteractUsers[]>{
      return Promise.resolve(this.interacts);
    }

    getFacebookLikes(id: number) : Promise<FacebookInteractUsers[]>{
      return Promise.resolve(this.interacts);
    }
}

export class EventsDummy {
  private events: FacebookEvent[] = [
    {id: 1, event:"Koncert Łąki Łan", owner: 'Łąki Łan', date: '06-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/255962424790662', status: 'Attend'},
    {id: 2, event:"Rap Festival", owner: 'Spodek Katowice', date: '16-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/1025874204192480', status: 'Respond'}
  ]
  getEventsStub() {
    return this.events;
  }

  getFacebookEvents():Promise<FacebookEvent[]>{
    return Promise.resolve(this.events);
  }
}

export class FacebookPostsDummy {
  private likeFlag = true;
  private facebookPosts =  [ {
      id: 1,
      date : "3 days ago",
      msg: "Pierwszy wpis na fejse - link do video",
      likes: 4,
      shares: 1,
      anwsers: [
        {
          id: 1,
          date: "10 minutes ago",
          user: "Janusz",
          userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
          msg: "Co jest kuwa?",
          likes: 2,
          replies: false
        },
        {
          id: 2,
          date: "12 minutes ago",
          user: "Janusz 2",
          userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
          msg: "Elo, elo",
          likes: 1,
          replies: true
        }
      ]
    },
    {
      id: 2,
      date: "3 hours ago",
      msg: "Drugi wpis - ty kupo",
      likes: 0,
      shares: 2,
      anwsers: []
    }
  ];


  private replies = [
    {
      id: 1,
      date: "10 minutes ago",
      user: "Bożena",
      userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
      msg: "O ty pierunie, wylaz z neta?",
      likes: 2,
    },
    {
      id: 2,
      date: "12 minutes ago",
      user: "Janusz",
      userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
      msg: "Ale bożena, jak to?",
      likes: 0,
    }
  ];

  getFBPostsStub() {
    return this.facebookPosts;
  }

  getFacebookPosts() : Promise<FacebookPost[]>{
    return Promise.resolve(this.facebookPosts);
  }

  getRepliesStub() {
    return this.replies;
  }

  getReplies(postId: number, anwserId: number) :  Promise<FacebookPostAnwser[]> {
    return Promise.resolve(this.replies);
  }

  addReplie(idInfo: any, replie: string) : Promise<FacebookPostAnwser> {
    let mockData = {
      date: "now",
      user: "ja",
      userPic: "https://65.media.tumblr.com/avatar_aa8f82d10a6d_128.png",
      msg: replie,
      likes: 0
    }
    return Promise.resolve(mockData);
  }

  addLike(postId: number, anwserId: number, replyId: number): Promise<any>{
    if(this.likeFlag) {
        this.likeFlag = false;
        return Promise.resolve(true);
    } else {
        this.likeFlag = true;
        return Promise.resolve(false);
    }
  }

  addComment(id: number, comment: string ) : Promise<any> {
    let mockData = {
      id: 3,
      date: "10 minutes ago",
      user: "Janusz",
      userPic: "janusz.png",
      msg: "ok",
      likes: 2,
      replies: false
    }
      return Promise.resolve(mockData);

  }


}
