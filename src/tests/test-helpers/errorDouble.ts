import { Observable } from 'rxjs';
import { Subject }    from 'rxjs/Subject';

export class ErrorDouble {
private errorSubject = new Subject<string>();
errorOccured$ = this.errorSubject.asObservable();


  startErrorAction(error: any) {

  }

  triggerErrorMsg() {
    this.errorSubject.next('An error occured');
  }

}
