import { Observable } from 'rxjs';
import { Subject }    from 'rxjs/Subject';


export class AuthLoginDouble {

  private userSubject = new Subject<boolean>();
  private userLogged = new Subject<string>();
  $userSubject = this.userSubject.asObservable();
  userLogged$ = this.userLogged.asObservable();
  // login(login: string, password: string): Observable<boolean> {
  //   if(login == 'test' && password == 'test123') {
  //     return Observable.create( observer => {
  //         observer.next(true);
  //         observer.complete();
  //       });
  //    }  else {
  //     return Observable.create( observer => {
  //         observer.next(false);
  //         observer.complete();
  //     });
  //   }
  // }
  logUser() : void {
    this.userLogged.next('user123');
  }

  login(login: string, password: string): Observable<boolean> {
    if(login == 'test' && password == 'test123') {
      this.userSubject.next(true);
     }  else {
       this.userSubject.next(false);
    }
    return this.$userSubject;
  }

  logout(): void {
      this.userLogged.next('');
  }
}
