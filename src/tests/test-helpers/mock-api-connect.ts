import { MockBackend } from '@angular/http/testing';
import { Response, ResponseOptions, ResponseType } from '@angular/http';

class MockError extends Response implements Error {
    name:any
    message:any
}

export class MockApiConnect{

  constructor(private mockBackend:MockBackend, private returnStub: any) {}

  createResponse() : void {
    this.mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(this.returnStub)
      })));
    });
  }

  createError() : void {
    let opts = {type:ResponseType.Error, status:404, body: JSON.stringify(this.returnStub)};
    let responseOpts = new ResponseOptions(opts);
    this.mockBackend.connections.subscribe((connection) => {
      connection.mockError(new MockError(responseOpts));
    }) ;
  }

}
