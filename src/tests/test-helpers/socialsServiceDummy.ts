import { SocialAccount} from './../../app/shared/models/social-account';
import { NewSocialAccount} from './../../app/shared/models/new-social-account';

export class SocialsServiceDummy {

    getSocials(): Promise<SocialAccount[]> {
      let socials =  [
        {id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true},
        {id: 5, social: "linkedin", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "LinkedinUser", active: true},
        {id: 6, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser", active: true}
      ];
      return Promise.resolve(socials);
    }

    addNewSocialAccount(newAccount: NewSocialAccount) : Promise<any>{
      let social4 = {id: 7, social: "facebook", userPic: "pic.jpg", login: "unusedLogin", active: true};
      return Promise.resolve(social4);
    }

    deleteAccount(id: number): Promise<void> {
      return Promise.resolve(null);
    }

    updateAccount(account: SocialAccount ):  Promise<void>{
        return Promise.resolve(null);
    }

}
