import { Component, OnInit, ViewChild } from  '@angular/core';
import { FormGroup, FormBuilder, Validators,  FormControl } from '@angular/forms';
import { ResponseOptions, Response } from '@angular/http';

import { AccountInteraction } from "./../shared/components/account-interaction/account-interaction.comp";
import { FacebookNewPost } from "./../shared/models/facebook-new-post";
import { FacebookPostAttachment } from "./../shared/models/facebook-post-attachment";
import { FacebookAddPostService } from './../services/facebook-add-post.service';
import { SocialsService } from './../services/socials.service';
import { SocialAccount } from './../shared/models/social-account';
import { isEmptyObject } from "./../shared/validators";
import { ErrorsService } from "./../services/errors.service";
import { SocialStreamChooserComponent } from './../shared/components/social-stream-chooser/social-stream-chooser.component';


@Component({
  selector: 'addpost',
  templateUrl: 'addpost.component.html',
  styleUrls: ['addpost.component.scss']
})

export class AddPostComponent extends AccountInteraction implements OnInit {
  @ViewChild('socialChooser') public socialChooser: SocialStreamChooserComponent;
  facebookNewPost: FormGroup;
  informationSend : boolean;
  newPostAddedSucces: boolean = false;
  maxMsglength: number = 63206;
  attachmentUploaded: boolean = false;

  constructor(
    private formbuilder: FormBuilder,
    private facebookAddPostService : FacebookAddPostService,
    private socialsService: SocialsService,
    private errorsService: ErrorsService
  ) { super()}

  ngOnInit(): void {
    this.facebookNewPost = this.formbuilder.group({
      account     : [{}, isEmptyObject],
      msg         : ['', Validators.maxLength(this.maxMsglength)],
      link        : ['', Validators.pattern('^https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,}$')],
      privacy     : ['', Validators.required],
      schedule    : ['', Validators.pattern('^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]$')]
    });
  }

  onSubmit() : void {
    if(this.checkOneOfThreeValidatorisTrue()) {
      this.facebookAddPostService.addFacebookPost(this.facebookNewPost.value)
        .then(
          () => { this.newPostAddedSucces = true;},
          error=> {
            this.errorsService.startErrorAction(error);
          }
        )
    }
  }

  private checkOneOfThreeValidatorisTrue() : boolean {
    let newPost : any = this.facebookNewPost.value;
    if((!newPost.msg) && (!newPost.link) && (!this.attachmentUploaded)) {
      this.informationSend = false;
      return false;
    }
    this.informationSend = true;
    return true;
  }

  selectAccount(account: SocialAccount) : void {
    super.selectAccount(account, this.facebookNewPost)
    this.changeMaxMsgLengthForTwitter();
  }

  removeAccountFromChoosed(account: SocialAccount) {
    super.removeAccountFromChoosed(account);
    this.socialChooser.addToUsedSocials(account);
  }

  private changeMaxMsgLengthForTwitter() : void {
    let values = [];
    for (var i in this.accountsStorage) {
      values.push(this.accountsStorage[i]);
    }

  //let values = Object.values(this.accountsStorage);
  if(values.includes("twitter")) {
      this.maxMsglength = 150;
    } else {
      this.maxMsglength = 63206;
    }
    this.facebookNewPost.controls['msg'].setValidators(Validators.maxLength(this.maxMsglength));
  }

  onUploadSuccess() {
    this.attachmentUploaded = true;
  }

  onUploadError() {
    this.attachmentUploaded = false;
    let options = new ResponseOptions({
      "status" : 404 , "statusText" : "There were a problem with image upload"
    });
    let error = new Response(options);
    this.errorsService.startErrorAction(error);
  }
}
