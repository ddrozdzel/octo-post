import { Component, OnInit } from "@angular/core";

import { ErrorsService } from './services/errors.service';

@Component({
  selector: 'error-message',
  template:
  `
    <div *ngIf = "errorMessage" (click)= "disableErrorMsg()"  (clickOutside)="disableErrorMsg()">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header text-center">
           <span>{{errorMessage}} </span>
        </div>
            <div class="modal-body text-center">
              <p> An internal error occured. We have been notified. Please try later </p>
            </div>
          </div>
        </div>
    </div>
  `
})

export class ErrorsComponent implements OnInit{
  errorMessage: string;

  constructor(private errorsService: ErrorsService) {}

  disableErrorMsg(): void{
    this.errorMessage = "";
  }


  ngOnInit(): void {
    this.errorsService.errorOccured$.subscribe(
      error => {this.errorMessage = error;}
    );
  }

}
