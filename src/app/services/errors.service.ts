import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Response } from '@angular/http';

import { LoggerService } from './logger.service';


@Injectable()

export class ErrorsService {
  private error: Response;
  private errorMessage: string;
  errorSingalSource = new Subject<string>();
  errorOccured$ = this.errorSingalSource.asObservable();

  constructor( private loggerService:LoggerService) {}

  public startErrorAction(error: Response) {
    this.error = error;
    this.setErrorHttpMsg();
    this.sendError();
  }

  get getError() : Response {
    return this.error;
  }

  get getErrorMessage() : string {
    return this.errorMessage;
  }

  private setErrorHttpMsg() : void {
    this.errorMessage = this.error.status + "\n" + this.error.statusText;
  }

  private sendError() : void {
    this.loggerService.logError(this.error);
    this.sendErrorSignal(this.errorMessage);
  }

  sendErrorSignal(errorMessage: string) : void {
    this.errorSingalSource.next(errorMessage);
  }

}
