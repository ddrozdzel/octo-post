import { Injectable } from '@angular/core';
import { Http  }  from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { SocialAccount } from "./../shared/models/social-account";
import { NewSocialAccount } from "./../shared/models/new-social-account";
import { UpdatedSocialAccount } from "./../shared/models/updated-social-account";
import { AuthLoginService } from './auth-login.service';
import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()

export class SocialsService extends HttpRequestService {
  private socialsUrl = "app/socials";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getSocials(): Promise<SocialAccount[]> {
    return this.http.get(this.socialsUrl)
                .toPromise()
                .then(response => response.json().data as SocialAccount[])
                .catch(this.handleError)
  }

  addNewSocialAccount(newAccount: NewSocialAccount) : Promise<any>{
    return this.http
      .post(this.socialsUrl, JSON.stringify(newAccount), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  deleteAccount(id: number): Promise<void> {
    const url = `${this.socialsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  updateAccount(account: any) : Promise<void> {
    let id = account.id;
    const url = `${this.socialsUrl}/${id}`;

    return this.http
               .put(url, JSON.stringify(account), {headers: this.headers})
               .toPromise()
               .then(() => null)
               .catch(this.handleError);
  }

}
