import { Injectable } from '@angular/core';
import { Http }    from '@angular/http';

import { FacebookPost } from "./../shared/models/facebook-post";
import { FacebookPostAnwser } from "./../shared/models/facebook-post-anwser";
import { AuthLoginService } from './auth-login.service';
import { HttpRequestService } from './../shared/services/http-request/http-request.service'


@Injectable()

export class FacebookPostsService extends HttpRequestService {
  private url = "app/facebookPosts";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getFacebookPosts(limit?: number) : Promise<FacebookPost[]>{
    //this.url + limit
    return this.http.get(this.url)
                .toPromise()
                .then(response => response.json().data as FacebookPost[])
                .catch(this.handleError)
  }

  addComment(id: number, comment: string ) : Promise<any> {
    let url = this.url + "/" + id + "/anwser";
    //console.log(url);
    return this.http
               .post(url, JSON.stringify(comment), {headers: this.headers})
               .toPromise()
               .then((res) => res.json().data)
               .catch(this.handleError);
  }

  addLike(postId: number, anwserId: number, replyId: number): Promise<any>{
    let url = "";
    if(replyId !== null) {
        url = this.url + "/" + postId + "/anwser/" + anwserId + "/replie/" + replyId + "/like";
    } else if(replyId === null && anwserId !== null ){
      url = this.url + "/" + postId + "/anwser/" + anwserId + "/like";
    } else {
        url = this.url + "/" + postId + "/like";
    }
    return this.http
              .get(url, {headers: this.headers})
              .toPromise()
              .then(() => true)
              .catch(this.handleError);
  }

  getReplies(postId: number, anwserId: number) :  Promise<FacebookPostAnwser[]> {
    let  url = this.url + "/" + postId + "/anwser/" + anwserId;
    let urlMock = "app/replies";
    return this.http.get(urlMock)
                .toPromise()
                .then(response => response.json().data as FacebookPostAnwser[])
                .catch(this.handleError)

  }

  addReplie(idInfo: any, replie: string) : Promise<FacebookPostAnwser> {
    let  url = this.url + "/" + idInfo.postId + "/anwser/" + idInfo.anwserId + "/replie";
    //console.log(url);
    let urlMock = "app/replies";
    let mockData = {
      date: "now",
      user: "ja",
      userPic: "https://65.media.tumblr.com/avatar_aa8f82d10a6d_128.png",
      msg: replie,
      likes: 0
    }
    return this.http
              .post(urlMock, JSON.stringify(replie), {headers: this.headers})
              .toPromise()
              .then(res => res.json().data)
              .catch(this.handleError);
  }


}
