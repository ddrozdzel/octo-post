import { Injectable } from '@angular/core';
import { Http  }    from '@angular/http';
import { AuthLoginService } from './auth-login.service';
import { FacebookInteractUsers } from "./../shared/models/facebook-interact-users";
import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()
export class FacebookLikesService extends HttpRequestService {
  private url = "app/facebookLikes";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getFacebookLikes(id: number) : Promise<FacebookInteractUsers[]>{
    //let url = `${this.url}/${id}`;
    let url = this.url;
    return this.http.get(url)
                .toPromise()
                .then(response => response.json().data as FacebookInteractUsers[])
                .catch(this.handleError)
  }
}
