import { Injectable } from '@angular/core';
import {  Http  }    from '@angular/http';
import { AuthLoginService } from './auth-login.service';
import { FacebookPostShare } from "./../shared/models/facebook-post-share";
import { HttpRequestService } from './../shared/services/http-request/http-request.service'
@Injectable()

export class FacebookPostShareService extends HttpRequestService{
  private url = "app/sharePost";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  shareFacebookPost(sharePost: FacebookPostShare) : Promise<any>{
    return this.http
                .post(this.url, JSON.stringify(sharePost), {headers: this.headers})
                .toPromise()
                .then(() => null)
                .catch(this.handleError);
  }
}
