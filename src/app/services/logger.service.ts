import { Injectable } from '@angular/core';
import {  Http  }    from '@angular/http';
import { AuthLoginService } from './auth-login.service';
import 'rxjs/add/operator/toPromise';

import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()
export class LoggerService  extends HttpRequestService  {
  private url = "app/logError";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  logError(error: any) : void{
   this.http.post(this.url, JSON.stringify(error), {headers: this.headers})
  }

}
