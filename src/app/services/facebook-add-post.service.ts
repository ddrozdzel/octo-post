import { Injectable } from '@angular/core';
import { Http  }    from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { FacebookNewPost } from "./../shared/models/facebook-new-post";
import { AuthLoginService } from './auth-login.service';
import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()
export class FacebookAddPostService extends HttpRequestService {
  private addPostUrl = "app/addPost";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  addFacebookPost(newPost: FacebookNewPost) : Promise<any>{
    return this.http
      .post(this.addPostUrl, JSON.stringify(newPost), {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
}
