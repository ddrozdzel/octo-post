import { Injectable } from '@angular/core';
import { Headers, Http  }  from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { AuthLoginService } from './auth-login.service';
import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()

export class UserInfoService extends HttpRequestService  {
  private socialsUrl = "app/userInfo";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getSocialsInfo() : Promise<string[]> {
    return this.http.get(this.socialsUrl)
                .toPromise()
                .then(response => response.json().data)
                .catch(this.handleError)

  }
}
