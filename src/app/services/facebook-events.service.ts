import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { HttpRequestService } from './../shared/services/http-request/http-request.service'
import { FacebookEvent } from './../shared/models/facebook-event';
import { AuthLoginService } from './auth-login.service';


@Injectable()

export class FacebookEventsService extends HttpRequestService {
  private url = "app/events";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getFacebookEvents() : Promise<FacebookEvent[]>{
    return this.http.get(this.url)
                .toPromise()
                .then(response => response.json().data as FacebookEvent[])
                .catch(this.handleError)
  }
}
