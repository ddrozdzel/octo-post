import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Subject }    from 'rxjs/Subject';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthLoginService {
    public token: string;
    private userSubject = new Subject<string>();
    userLogged$ = this.userSubject.asObservable();
    
    constructor(private http: Http) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(login: string, password: string): Observable<boolean> {
        let logData =  JSON.stringify({ login: login, password: password });
        return this.http.post('/login', logData)
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                if (token) {
                    this.token = token;
                    this.userSubject.next(login);
                    localStorage.setItem('currentUser',   JSON.stringify({ login: login, token: token  }));
                    return true;
                } else {
                    return false;
                }
            });
    }

    logout(): void {
        this.userSubject.next('');
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}
