import { Injectable } from '@angular/core';
import { Http }    from '@angular/http';
import { AuthLoginService } from './auth-login.service';
import { FacebookInteractUsers } from "./../shared/models/facebook-interact-users";
import { HttpRequestService } from './../shared/services/http-request/http-request.service'

@Injectable()
export class FacebookSharesService extends HttpRequestService {
  private url = "app/facebookShares";

  constructor(
   protected http: Http,
   protected authLoginService:AuthLoginService
  ) {
    super(http, authLoginService);
  }

  getFacebookShares(id: number) : Promise<FacebookInteractUsers[]>{
        //let url = `${this.url}/${id}`;
    return this.http.get(this.url)
                .toPromise()
                .then(response => response.json().data as FacebookInteractUsers[])
                .catch(this.handleError)
  }
}
