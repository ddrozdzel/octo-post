import {NgModule} from '@angular/core'
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { HttpModule }    from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {rootRouterConfig} from "./app.routes";
import { AppComponent } from './app'
import { ErrorsComponent } from './errors.component';
import { DashboardComponent } from './dashboard/dashboard-main/dashboard.component';
import { FacebookLikesComponent } from "./dashboard/facebook-elements/likes/facebook-likes.component";
import { FacebookSharesComponent } from "./dashboard/facebook-elements/shares/facebook-shares.component";
import { FacebookPostShareComponent } from "./dashboard/facebook-elements/post-share/facebook-post-share.component";
import { AddPostComponent } from './addpost/addpost.component';
import { SocialsComponent } from './socials/socials.component';
import { SocialEditComponent } from "./socials/social-edit/social-edit.component";
import { FacebookRepliesComponent } from "./dashboard/facebook-elements/replies/facebook-replies.component";
import { FacebookEventsComponent } from "./dashboard/facebook-elements/events/facebook-events.component";
import { FacebookComponent } from "./dashboard/facebook-elements/facebook/facebook.component";
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

//------------------------  symulator web api --------------------//
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './mock_data/in-memory-data.service';

// import { fakeBackendProvider } from './mock_data/loginMock';
// import { MockBackend, MockConnection } from '@angular/http/testing';
// import { BaseRequestOptions } from '@angular/http';

//------------------------  symulator web api --------------------//

import { ErrorsService } from './services/errors.service';
import { LoggerService } from './services/logger.service';
import { FacebookPostsService } from './services/facebook-posts.service';
import { FacebookLikesService } from './services/facebook-likes.service';
import { FacebookSharesService } from './services/facebook-shares.service';
import { FacebookAddPostService } from './services/facebook-add-post.service';
import { FacebookPostShareService } from "./services/facebook-post-share.service";
import { FacebookEventsService } from "./services/facebook-events.service";
import { SocialsService } from './services/socials.service';
import { SocialStreamChooserComponent } from './shared/components/social-stream-chooser/social-stream-chooser.component';
import { AccordionAccountComponent } from './shared/components/accordion-account/accordion-account.component';
import { SocialLoginSearchPipe } from './shared/social-login-search.pipe';
import { SafeUrlPipe } from './shared/safe-url.pipe';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthLoginService } from './services/auth-login.service';
import { DROPZONE_CONFIG } from './shared/dropzone-config';

//------------------------- extarnal modules ---------------------//
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker/src';
import { ClickOutsideModule } from 'ng2-click-outside';
import { MaterialModule } from '@angular/material';
import { ModalModule } from 'ng2-bootstrap';
import { AccordionModule } from 'ng2-bootstrap';
import { DropzoneModule } from 'angular2-dropzone-wrapper';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';





//------------------------- extarnal modules ---------------------//

@NgModule({
  imports     : [
    BrowserModule,
    RouterModule.forRoot(rootRouterConfig),
    HttpModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    Ng2DatetimePickerModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    InfiniteScrollModule ,
    ModalModule.forRoot() ,
    AccordionModule.forRoot(),
    DropzoneModule.forRoot(DROPZONE_CONFIG),
    //------------------------  symulator web api --------------------//
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    //------------------------  symulator web api --------------------//
  ],
  declarations: [AppComponent, LoginComponent, ErrorsComponent, DashboardComponent, FacebookComponent, FacebookLikesComponent, FacebookSharesComponent, AddPostComponent, SocialsComponent, SocialEditComponent, FacebookRepliesComponent, FacebookPostShareComponent, FacebookEventsComponent, SocialStreamChooserComponent, SocialLoginSearchPipe, AccordionAccountComponent, PageNotFoundComponent, SafeUrlPipe],
  providers: [ErrorsService, LoggerService, FacebookPostsService, FacebookLikesService, FacebookSharesService, FacebookAddPostService, SocialsService, FacebookPostShareService, FacebookEventsService, AuthGuardService, AuthLoginService
    // providers used to create fake backend
    // ,fakeBackendProvider,
    // MockBackend,
    // BaseRequestOptions
  ],
  bootstrap   : [AppComponent]
})
export class AppModule {}
