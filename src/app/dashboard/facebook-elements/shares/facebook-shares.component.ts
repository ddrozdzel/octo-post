import { Component, Input, OnChanges, SimpleChange, EventEmitter, Output} from "@angular/core";

import { FacebookInteractUsers } from "./../../../shared/models/facebook-interact-users";
import { FacebookPost } from "./../../../shared/models/facebook-post";
import { FacebookSharesService } from "./../../../services/facebook-shares.service";
import { ErrorsService } from "./../../../services/errors.service";

@Component ({
    selector: "facebook-shares",
    templateUrl: "../interact-users.template.html"
})

export class FacebookSharesComponent implements OnChanges{
  @Input() selectedPostShares: FacebookPost;
  @Output() disableShares = new EventEmitter();

  facebookInteractUsers: FacebookInteractUsers[];

  constructor(
    private facebookSharesService: FacebookSharesService,
    private errorsService: ErrorsService
  ) {}

  ngOnChanges(changes:any) {
    if(this.selectedPostShares) {
      this.getFacebookShares();
      }
   }

   getFacebookShares(): void {
     let id = +(this.selectedPostShares.id);
     this.facebookSharesService.getFacebookShares(id).then(
       fbShares => {this.facebookInteractUsers = fbShares},
       error => {
         this.errorsService.startErrorAction(error);
         this.disableSelectedPost();
       }
     );
   }

  disableSelectedPost() : void {
    this.disableShares.emit();
  }

}
