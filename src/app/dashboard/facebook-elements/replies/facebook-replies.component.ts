import { Component } from "@angular/core";
import { FacebookPostsService } from "./../../../services/facebook-posts.service";
import { ErrorsService } from "./../../../services/errors.service";
import { FacebookPostAnwser } from "./../../../shared/models/facebook-post-anwser";

@Component({
  selector: "facebook-replies",
  templateUrl: "facebook-replies.component.html"
})

export class FacebookRepliesComponent {
  replies: FacebookPostAnwser[];
  idInfo : any;

  constructor(
    private facebookPostsService: FacebookPostsService,
    private errorsService: ErrorsService
  ) {  }

  showReplies(postId: number, anwserId: number) : void {
    this.idInfo = {
      postId  : postId,
      anwserId: anwserId
    }
    this.facebookPostsService.getReplies(postId, anwserId).then(
      fbReplies => {this.replies = fbReplies},
      error => {  this.errorsService.startErrorAction(error) }
    );
  }

  makeReplie(replie: HTMLInputElement): void {
    this.facebookPostsService.addReplie(this.idInfo, replie.value).then(
      newReply => {this.replies.push(newReply)},
      error => {  this.errorsService.startErrorAction(error) }
    );
    replie.value = null;
  }

  addReplieLike(replieId : number) : void {
    this.facebookPostsService.addLike(this.idInfo.postId, this.idInfo.anwserId, replieId).then(
      like => {
        let index =this.replies.findIndex(obj => obj.id === replieId);
        like === true ? this.replies[index].likes++ : this.replies[index].likes--;
      },
      error => {  this.errorsService.startErrorAction(error) }
     );
  }

}
