import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { FacebookPostsService } from "./../../../services/facebook-posts.service";

import { ModalDirective } from 'ng2-bootstrap';

import { FacebookPost } from "./../../../shared/models/facebook-post";
import { FacebookRepliesComponent } from "./../replies/facebook-replies.component";
import { ErrorsService } from "./../../../services/errors.service";
import { SocialAccount } from './../../../shared/models/social-account';

@Component({
  selector: "facebook",
  templateUrl: "facebook.component.html",
  styleUrls: ['facebook.component.css']
})

export class FacebookComponent implements OnInit {
  @Input() facebookAccount: SocialAccount;
  @ViewChild('postShareModal') public postShareModal:ModalDirective;
  @ViewChild('showSharesModal') public showSharesModal:ModalDirective;
  @ViewChild('showLikesModal') public showLikesModal:ModalDirective;
  @ViewChild('showEvents') public showEvents:ModalDirective;
  @Output() closeTab = new EventEmitter();
  facebookPosts: FacebookPost[];
  facebookLikesPost: FacebookPost;
  facebookSharesPost: FacebookPost;
  facebookPostShare: FacebookPost;
  showFacebookEvents: boolean;
  private scrollDownCounter: number = 1;


  constructor(
    private facebookPostsService: FacebookPostsService,
    private errorsService: ErrorsService
   ) {}

   ngOnInit(): void {
     this.getFacebookPosts();
   }

   closeFacebookTab() {
     this.closeTab.emit();
   }

   getFacebookPosts(): void {
     this.facebookPostsService.getFacebookPosts().then(
       facebookPosts => {this.facebookPosts = facebookPosts},
       error => {  this.errorsService.startErrorAction(error) }
     );
   }

   showFacebookLikes(post: FacebookPost) : void {
     this.facebookLikesPost = post;
   }

   disableLikes(): void {
     this.facebookLikesPost = null;
     this.showLikesModal.hide();
   }

   showFacebookShares(post: FacebookPost) : void {
     this.facebookSharesPost = post;
   }

   disableShares(): void {
     this.facebookSharesPost = null;
     this.showSharesModal.hide();
   }

   showFbEvents() : void {
     this.showFacebookEvents = true;
   }

   sharePost(post: FacebookPost) : void {
     this.facebookPostShare = post;
   }


   onPostShared() : void {
     this.facebookPostShare = null;
     this.postShareModal.hide();
   }

  addComment(id: number, comment: HTMLInputElement) : void {
    this.facebookPostsService.addComment(+id, comment.value).then( anwser => {
        let index = this.facebookPosts.findIndex(obj => obj.id === id);
        this.facebookPosts[index].anwsers.push(anwser);

      },
      error => {  this.errorsService.startErrorAction(error)
     });
    comment.value = null;
  }

  addLike(postId : number) : void {
    this.facebookPostsService.addLike(postId, null, null).then( like => {
        let index = this.facebookPosts.findIndex(obj => obj.id === postId);
        like === true ? this.facebookPosts[index].likes++ : this.facebookPosts[index].likes--;
      },
      error => {  this.errorsService.startErrorAction(error)
     });
  }

  addAnwserLike(postId : number, anwserId: number) : void {
    this.facebookPostsService.addLike(postId, anwserId, null).then( like => {
      let index =this.facebookPosts.findIndex(obj => obj.id === postId);
      let anwserIndex = this.facebookPosts[index].anwsers.findIndex(obj => obj.id === anwserId);
      like === true ? this.facebookPosts[index].anwsers[anwserIndex].likes++ : this.facebookPosts[index].anwsers[anwserIndex].likes--;
    },
    error => {  this.errorsService.startErrorAction(error)
   });
  }

  onScrollDown() : void {
    this.facebookPostsService.getFacebookPosts(this.scrollDownCounter).then(
      facebookPosts => {
        let higherPosts = this.facebookPosts;
        this.facebookPosts = higherPosts.concat(facebookPosts);
        this.scrollDownCounter++;
      },
      error => {  this.errorsService.startErrorAction(error) }
    );
  }

}
