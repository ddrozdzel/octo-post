import { Component, Input, OnChanges, SimpleChange, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

import { AccountInteraction } from "./../../../shared/components/account-interaction/account-interaction.comp";
import { SocialStreamChooserComponent } from './../../../shared/components/social-stream-chooser/social-stream-chooser.component';
import { FacebookPostShareService } from './../../../services/facebook-post-share.service';
import { SocialAccount } from './../../../shared/models/social-account';
import { FacebookPostShare } from "./../../../shared/models/facebook-post-share";
import { isEmptyObject } from "./../../../shared/validators";
import { ErrorsService } from "./../../../services/errors.service";

@Component({
    selector: "facebook-post-share",
    templateUrl: "facebook-post-share.component.html"
})

export class FacebookPostShareComponent extends AccountInteraction implements OnChanges {
    @ViewChild('socialChooser') public socialChooser: SocialStreamChooserComponent;
    @Input() sharePost;
    @Output() postShared = new EventEmitter();
    newSharedPost: FormGroup;


    constructor(
      private formbuilder: FormBuilder,
      private facebookPostShareService: FacebookPostShareService,
      private errorsService: ErrorsService
    ){ super()}

    ngOnChanges(changes:any) {
      if(this.sharePost) {
        this.newSharedPost = this.formbuilder.group({
            account   : [{}, isEmptyObject],
            comment   : ['', Validators.maxLength(63206)],
            privacy   : ['', Validators.required]
        });
      }
    }

    onSubmit() : void {
      this.newSharedPost.value['postId'] = this.sharePost.id;
      this.facebookPostShareService.shareFacebookPost(this.newSharedPost.value)
      .then(() => {
         this.postShared.emit(); },
         error => {
          this.errorsService.startErrorAction(error);
          this.postShared.emit();
         }
      )
    }

    selectAccount(account: SocialAccount) : void {
      super.selectAccount(account, this.newSharedPost)
    }

    removeAccountFromChoosed(account: SocialAccount) {
      super.removeAccountFromChoosed(account);
      this.socialChooser.addToUsedSocials(account);
    }

}
