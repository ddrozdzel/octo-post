import { Component, OnInit, ViewChild, Input} from "@angular/core";
import { FacebookEventsService } from './../../../services/facebook-events.service';
import { FacebookEvent } from './../../../shared/models/facebook-event';
import { ErrorsService } from "./../../../services/errors.service";
import { SocialAccount } from './../../../shared/models/social-account';

import { ModalDirective } from 'ng2-bootstrap';


@Component({
  selector: 'facebook-events',
  templateUrl: 'facebook-events.component.html'
})

export class FacebookEventsComponent implements OnInit{
  @Input() facebookAccountData: SocialAccount;

  facebookEvents: FacebookEvent[];

  constructor(
    private facebookEventsService: FacebookEventsService,
    private errorsService: ErrorsService
  ) {}


  ngOnInit() : void {
    this.facebookEventsService.getFacebookEvents().then(
      events=>{ this.facebookEvents = events },
      error=> {  this.errorsService.startErrorAction(error) }
    );
  }



}
