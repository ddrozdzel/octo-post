import { Component, Input,Output, OnChanges, SimpleChange, EventEmitter} from "@angular/core";

import { FacebookInteractUsers } from "./../../../shared/models/facebook-interact-users";
import { FacebookPost } from "./../../../shared/models/facebook-post";
import { FacebookLikesService } from "./../../../services/facebook-likes.service";
import { ErrorsService } from "./../../../services/errors.service";

@Component ({
    selector: "facebook-likes",
    templateUrl: "../interact-users.template.html"
})

export class FacebookLikesComponent implements OnChanges{
  @Input() selectedPostLikes: FacebookPost;
  @Output() disableLikes = new EventEmitter();
  facebookInteractUsers: FacebookInteractUsers[];

  constructor(
    private facebookLikesService: FacebookLikesService,
    private errorsService: ErrorsService
   ) {  }

   ngOnChanges(changes:any) {
     if(this.selectedPostLikes) {
       this.getFacebookLikes();
       }
    }

    getFacebookLikes(): void {
      let id = +(this.selectedPostLikes.id);
      this.facebookLikesService.getFacebookLikes(id)
      .then(
        fbLikes => {this.facebookInteractUsers = fbLikes},
        error=> {
          this.errorsService.startErrorAction(error);
          this.disableSelectedPost();
        }
      );
    }

    disableSelectedPost() : void {
      this.disableLikes.emit();
    }
}
