import { Component, OnInit, ViewChild} from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'
import { SocialAccount } from './../../shared/models/social-account';
import { SocialStreamChooserComponent } from './../../shared/components/social-stream-chooser/social-stream-chooser.component';


@Component({
  selector: "dashboard",
  templateUrl: "dashboard.component.html",
  styleUrls: ['dashboard.component.scss'],
  animations: [
    trigger('socialStreamState', [
       state('in', style({transform: 'translateX(0)'})),
       transition('void => *', [
         style({transform: ' scale(0.1,1)'}),
         animate(400)
       ]),
       transition('* => void', [
         animate(400, style({transform: 'scale(0.1,1'}))
       ])
     ])
  ]
})

export class DashboardComponent implements OnInit {
  activeStreams: SocialAccount[] = [];
  chooseSocialStream: boolean;
  @ViewChild(SocialStreamChooserComponent) public socialstreamChooser: SocialStreamChooserComponent;

  constructor() {}

  ngOnInit(): void {
    //localStorage.removeItem('activeSocialStreams');
    if(localStorage.getItem('activeSocialStreams')) {
      let activeStreams = JSON.parse(localStorage.getItem('activeSocialStreams'));
      activeStreams.forEach((account) => {
          this.onSelectedAccount(account);
      })
    }
  }

  openSocialStreamPopup() : void {
    this.chooseSocialStream = true;
  }

  closeSocialStreamPopup(e?: Event) : void {
    if(e !== undefined) {
     if (e.target['id'] == "plusTrigger" || e.target['className'].indexOf("dontCloseIfChoosed") != "-1" ) {
        return;
      }
    }
    this.chooseSocialStream = false;
  }

  onSelectedAccount(account: SocialAccount) {
    this.activeStreams.push(account);
    this.setLocalStorageActiveSocialStream();
  }

  closeSocialTab(account: SocialAccount) {
      this.removeFromActiveStream(account);
      this.pushToSocialStreamChooserUsedSocials(account);
      this.setLocalStorageActiveSocialStream();
  }

  private setLocalStorageActiveSocialStream():void {
    localStorage.setItem('activeSocialStreams', JSON.stringify(this.activeStreams));
  }

  pushToSocialStreamChooserUsedSocials(account: SocialAccount) : void {
    this.socialstreamChooser.usedSocials.push(account);
  }

  removeFromActiveStream(account: SocialAccount) {
    this.activeStreams = this.activeStreams.filter(function( obj ) {
      return obj.id !== account.id;
    });
  }

}
