export class FacebookEvent {
  id: number;
  event: string;
  owner: string;
  date: string;
  respondUrl: string;
  status: string;
}
