import { FacebookPostAnwser } from "./Facebook-post-anwser";

export class FacebookPost {
  id: number;
  date: string;
  msg: string;
  likes: number;
  shares: number;
  anwsers: FacebookPostAnwser[];
}

// export class FacebookPostAnwser {
//   id: number;
//   date: string;
//   user: string;
//   userPic: string;
//   msg: string;
//   likes: number;
//   replies: boolean;
// }
