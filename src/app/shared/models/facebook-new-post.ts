import { FacebookPostAttachment } from "./facebook-post-attachment";

export class FacebookNewPost {
  msg: string;
  link: string;
  attachments: FacebookPostAttachment[];
  privacy: string;
  schedule: string;
}
