export class SocialAccount {
  id: number;
  social: string;
  userPic: string;
  login: string;
  active: boolean;
}
