import { Headers, Http  }    from '@angular/http';
import { AuthLoginService } from './../../../services/auth-login.service';

export class HttpRequestService {

  protected headers = new Headers({'Content-Type': 'application/json',  'Authorization': 'Bearer ' + this.authLoginService.token});

  constructor(
    protected http: Http,
    protected authLoginService:AuthLoginService
  ) {}

  protected handleError(error: any): Promise<any> {
    return Promise.reject(error);
  }


}
