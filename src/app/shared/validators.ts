import { FormControl } from '@angular/forms';

interface Validator<T extends FormControl> {
   (c:T): {[error: string]:any};
}

export function isEmptyObject(control: FormControl) {
  return Object.keys(control.value).length !== 0 ? null : {
      isEmptyArray: {
        valid: false
      }
    };
}

export function areEqual(passwordKey: string, confirmPasswordKey: string) {
  return (group: any): {[key: string]: any} => {
    let password = group.controls[passwordKey];
    let confirmPassword = group.controls[confirmPasswordKey];
    if (password.value !== confirmPassword.value) {
      return {
        areNotEquals: true
      };
    }
  }
}
