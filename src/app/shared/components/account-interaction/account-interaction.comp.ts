import { SocialAccount } from './../../../shared/models/social-account';


export class AccountInteraction {
  accountsStorage: any = {};
  choosedAccounts: SocialAccount[] = [];
  privacies = ["Only Me", "Friends", "Friends of My Friends", "Public", "Other"];
  constructor(){}

  selectAccount(account: SocialAccount, patchedProperty: any) : void {
    let id = '' + account.id;
      if(!(id in this.accountsStorage)) {
       this.accountsStorage[id] = account.social;
       this.choosedAccounts.push(account);
      } else {
        this.removeAccountFromChoosed(account);
      }
      patchedProperty.patchValue({
        account: this.accountsStorage
      })
  }

  removeAccountFromChoosed(account: SocialAccount) {
    this.choosedAccounts = this.choosedAccounts.filter(function( obj ) {
      return obj.id !== account.id;
    });
    delete this.accountsStorage[account.id];
  }
}
