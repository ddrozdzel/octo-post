import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { SocialsService } from './../../../services/socials.service';
import { ErrorsService } from "./../../../services/errors.service";
import { SocialAccount } from './../../../shared/models/social-account';

@Component ({
  selector: "social-stream-chooser",
  templateUrl: "social-stream-chooser.component.html",
  styleUrls: ['social-stream-chooser.component.scss'],
})

export class SocialStreamChooserComponent implements OnInit {
  usedSocials: SocialAccount[];
  @Output() selectedAccount = new EventEmitter<SocialAccount>();

  constructor(
    private socialsService: SocialsService,
    private errorsService: ErrorsService,
  ) {}

  ngOnInit() {
    this.getUsedSocials();
  }

  private getUsedSocials() : void {
    this.socialsService
        .getSocials()
        .then(
          socials => {this.usedSocials = socials},
          error=> {
            this.errorsService.startErrorAction(error);
          }
        );
  }

  selectAccount(account: SocialAccount) {
      this.removeSocialFromAvailable(account);
      this.selectedAccount.emit(account);
  }

  private removeSocialFromAvailable(account: SocialAccount) {
    this.usedSocials = this.usedSocials.filter(function( obj ) {
      return obj.id !== account.id;
    });
  }

  addToUsedSocials(account: SocialAccount) {
    this.usedSocials.push(account);
    if(this.usedSocials) {
       this.usedSocials.sort(function(a, b){
        return a.login.localeCompare(b.login);
      });
    }
  }
}
