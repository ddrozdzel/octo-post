import { Component, EventEmitter, Output, Input } from  '@angular/core';
import { SocialAccount } from './../../models/social-account';

@Component({
  selector: 'accordion-account',
  templateUrl: 'accordion-account.component.html'
})

export class AccordionAccountComponent {
  @Input() choosedAccounts: SocialAccount[] ;
  @Output() removedAccount = new EventEmitter<SocialAccount>();

  removeAccountFromChoosed(account: SocialAccount) {
    this.removedAccount.emit(account);
  }

}
