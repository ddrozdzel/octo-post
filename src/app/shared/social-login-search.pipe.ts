import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "socialLoginSearch"
})

export class SocialLoginSearchPipe implements PipeTransform {
    transform(value: any, args: string): any {
       let filter = args.toLocaleLowerCase();
       return filter != '' ? value.filter(account=> account.login.toLocaleLowerCase().indexOf(filter) != -1) : value;
    }
}
