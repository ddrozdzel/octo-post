import { DropzoneConfigInterface } from 'angular2-dropzone-wrapper';

export const DROPZONE_CONFIG: DropzoneConfigInterface = {
  server: 'https://123httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*',
  thumbnailWidth: 250,
  thumbnailHeight: 250
};
