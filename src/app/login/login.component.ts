import { Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { areEqual } from "./../shared/validators";
import { AuthLoginService} from './../services/auth-login.service';
import { ErrorsService } from "./../services/errors.service";

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    user:  FormGroup;
    wrongLoginPass: boolean = false;

    constructor(
        private errorsServcice: ErrorsService,
        private router: Router,
        private authLoginService:  AuthLoginService,
        private formbuilder: FormBuilder
        ) { }

    ngOnInit() {
        this.authLoginService.logout();
        this.createNewFormPlain();
    }

    private createNewFormPlain() : void{
      this.user = this.formbuilder.group({
        login          : ['', Validators.required],
        password       : ['', [Validators.required, Validators.minLength(6)]]
      })
    }

    onSubmit() : void {
      this.login();
    }

    private login() : void {
      this.authLoginService.login(this.user.value.login, this.user.value.password)
          .subscribe(result => {
              if (result === true) {
                  this.wrongLoginPass = false;
                  this.createNewFormPlain();
                  this.router.navigate(['/dashboard']);
              } else {
                  this.wrongLoginPass=true;
              }
          });
    }
}
