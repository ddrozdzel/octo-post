import {Component, ViewContainerRef, ViewEncapsulation, OnInit } from '@angular/core';
import {AuthLoginService} from './services/auth-login.service';


@Component({
  selector   : 'app',
  templateUrl: './app.html',
  styleUrls: ["app.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  private viewContainerRef: ViewContainerRef;
  user: string;

 public constructor(
   viewContainerRef:ViewContainerRef,
   private authLoginService: AuthLoginService
  ) {
   this.viewContainerRef = viewContainerRef;
 }

  ngOnInit(): void {
    this.authLoginService.userLogged$.subscribe(
      user => {this.user = user;}
    );
  }

}
