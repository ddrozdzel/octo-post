import { Component, Input, OnChanges, SimpleChange,  EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SocialsService } from './../../services/socials.service';
import { ErrorsService } from "./../../services/errors.service";
import { SocialAccount } from './../../shared/models/social-account';
import { UpdatedSocialAccount } from "./../../shared/models/updated-social-account";
import { areEqual } from "./../../shared/validators";

@Component({
  selector: "social-edit",
  templateUrl: "social-edit.component.html"
})

export class SocialEditComponent implements OnChanges {

 @Input() account: SocialAccount;
 accountEdited: boolean = false;
 updatedSocialAccount: FormGroup;

  constructor(
      private formbuilder: FormBuilder,
      private socialsService: SocialsService,
      private errorsService: ErrorsService
    ) {}

    //ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    ngOnChanges(changes:any) {
      if(this.account) {
       this.updatedSocialAccount = this.formbuilder.group({
         id              : [this.account.id, Validators.required],
         login           : [this.account.login , Validators.required],
         password        : ['', [Validators.required, Validators.minLength(6)]],
         passwordConfirm : ['', [Validators.required, Validators.minLength(6)]]
       }, {validator: areEqual('password', 'passwordConfirm')
       })
     }
   }

  onSubmit() : void {
    this.cleanUpUpdatedAccount();
    this.socialsService.updateAccount(this.updatedSocialAccount.value)
    .then(
      () => { this.accountEdited = true},
      error => {
        this.errorsService.startErrorAction(error);
      }
    )
  }

  private cleanUpUpdatedAccount() : void {
    delete this.updatedSocialAccount.value.passwordConfirm;
  }

  resetEdition() : void {
    this.accountEdited = false;
  }
  
}
