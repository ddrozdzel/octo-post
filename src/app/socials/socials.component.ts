import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router }            from '@angular/router';

import { ModalDirective } from 'ng2-bootstrap';

import { SocialsService } from './../services/socials.service';
import { SocialAccount } from './../shared/models/social-account';
import { NewSocialAccount } from "./../shared/models/new-social-account";
import { ErrorsService } from "./../services/errors.service";
import { areEqual } from "./../shared/validators";

@Component({
  selector: "socials",
  templateUrl: "socials.component.html"
})

export class SocialsComponent implements OnInit{
  @ViewChild('accountAlreadyTaken') public accountAlreadyTaken:ModalDirective;
  accountCreated: boolean = false;
  usedSocials: SocialAccount[];
  newSocialAccount: FormGroup;
  socialsSites: string[] = ["facebook", "twitter", "linkedin"];

  constructor(
    private formbuilder: FormBuilder,
    private socialsService: SocialsService,
    private router: Router,
    private errorsService: ErrorsService
  ) {}

  ngOnInit() : void {
    this.getUsedSocials();
    this.createNewFormPlain();
  }

  private getUsedSocials() : void {
    this.socialsService
        .getSocials()
        .then(
          socials => {this.usedSocials = socials},
          error =>  { this.errorsService.startErrorAction(error) }
        );
  }

  private createNewFormPlain() : void{
    this.newSocialAccount = this.formbuilder.group({
      social         : ['', Validators.required],
      login          : ['', Validators.required],
      password       : ['', [Validators.required, Validators.minLength(6)]],
      passwordConfirm : ['', [Validators.required, Validators.minLength(6)]]
    }, {validator: areEqual('password', 'passwordConfirm')
    })
  }

  onSubmit() : void {
    this.deletePasswordConfirm() ;
    if(this.checkIfAccountIsAlreadyUsed()) {
      this.socialsService.addNewSocialAccount(this.newSocialAccount.value)
        .then(
          newAccount => {
            this.usedSocials.push(newAccount),
            this.accountCreated = true;
           },
          error =>  { this.errorsService.startErrorAction(error) }
        )
    }
  }

  private deletePasswordConfirm() : void {
    delete this.newSocialAccount.value.passwordConfirm;
  }

  private checkIfAccountIsAlreadyUsed() : boolean {
      let social = this.newSocialAccount.value.social;
      let login = this.newSocialAccount.value.login;
      for(var i=0; i < this.usedSocials.length; i++) {
        if(this.usedSocials[i].social == social && this.usedSocials[i].login == login) {
          this.accountAlreadyTaken.show();
          return false;
        }
      }
      return true;
  }

  deleteAccount(account: SocialAccount): void {
    this.socialsService
      .deleteAccount(account.id)
      .then(
        () => {this.usedSocials = this.usedSocials.filter(h => h !== account);},
        error => {this.errorsService.startErrorAction(error)}
    );
  }

  resetNewAccountForm() : void {
    this.createNewFormPlain();
    this.accountCreated = false;
  }

}
