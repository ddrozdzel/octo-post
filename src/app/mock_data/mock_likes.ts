import { FacebookInteractUsers } from "../shared/models/facebook-interact-users";

export const MOCK_LIKES: FacebookInteractUsers[] = [
  {
    user: "Jasiek",
    userPic: "https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
  },
  {
    user: "Marek",
    userPic: "https://resize.goldenline.io/1/display/resize?url=https%3A%2F%2Fstatic.goldenline.pl%2Fuser_photo%2F217%2Fuser_4653273_e968f6_huge.jpg&width=50&height=50&key=d4088ba7a08d4f8fb8c38f03db3949e4"
  }
];

export const MOCK_LIKES2: FacebookInteractUsers[] = [
  {
    user: "Trolololo",
    userPic: "https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
  },
  {
    user: "Tracz",
    userPic: "https://resize.goldenline.io/1/display/resize?url=https%3A%2F%2Fstatic.goldenline.pl%2Fuser_photo%2F217%2Fuser_4653273_e968f6_huge.jpg&width=50&height=50&key=d4088ba7a08d4f8fb8c38f03db3949e4"
  }
];
