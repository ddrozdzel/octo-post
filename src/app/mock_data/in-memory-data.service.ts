import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {


  createDb() {

    let replies = [
      {
        id: 1,
        date: "10 minutes ago",
        user: "Bożena",
        userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
        msg: "O ty pierunie, wylaz z neta?",
        likes: 2,
      },
      {
        id: 2,
        date: "12 minutes ago",
        user: "Janusz",
        userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
        msg: "Ale bożena, jak to?",
        likes: 0,
      }
];


    let facebookPosts =  [ {
        id: 1,
        date : "3 days ago",
        msg: "Pierwszy wpis na fejse - link do video <a href='https://www.youtube.com/watch?v=m_dYHji-bME'>https://www.youtube.com/watch?v=m_dYHji-bME       </a>",
        likes: 4,
        shares: 1,
        anwsers: [
          {
            id: 1,
            date: "10 minutes ago",
            user: "Janusz",
            userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
            msg: "Co jest kuwa?",
            likes: 2,
            replies: false
          },
          {
            id: 2,
            date: "12 minutes ago",
            user: "Janusz 2",
            userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg",
            msg: "Elo, elo",
            likes: 1,
            replies: true
          }
        ]
      },
      {
        id: 2,
        date: "3 hours ago",
        msg: "Drugi wpis - ty kupo",
        likes: 0,
        shares: 2,
        anwsers: []
      }
    ];

    let facebookShares = [
      {
        user: "ShareUser1",
        userPic: "https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
      },
      {
        user: "ShareUser2",
        userPic: "https://resize.goldenline.io/1/display/resize?url=https%3A%2F%2Fstatic.goldenline.pl%2Fuser_photo%2F217%2Fuser_4653273_e968f6_huge.jpg&width=50&height=50&key=d4088ba7a08d4f8fb8c38f03db3949e4"
      }
    ];


    let facebookLikes = [
      {
        user: "Jasiek",
        userPic: "https://yt3.ggpht.com/-lDILAKhijxU/AAAAAAAAAAI/AAAAAAAAAAA/fwXCuEqo9iM/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
      },
      {
        user: "Marek",
        userPic: "https://resize.goldenline.io/1/display/resize?url=https%3A%2F%2Fstatic.goldenline.pl%2Fuser_photo%2F217%2Fuser_4653273_e968f6_huge.jpg&width=50&height=50&key=d4088ba7a08d4f8fb8c38f03db3949e4"
      }
    ];


    let socials = [
      {id: 1, social: "twitter", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "TwitterUser", active: true},
      {id: 5, social: "linkedin", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "LinkedinUser", active: true},
      {id: 6, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser", active: true},
      {id: 7, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser2", active: true},
      {id: 8, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser3", active: true},
      {id: 9, social: "facebook", userPic: "https://yt3.ggpht.com/-T60siaYF2ZQ/AAAAAAAAAAI/AAAAAAAAAAA/1dxnGYkntPI/s48-c-k-no-mo-rj-c0xffffff/photo.jpg", login: "FacebookUser4", active: true}
    ];

    let addPost = [];
    let sharePost = [];

    let logError = [];

    let events = [
      {id: 1, event:"Koncert Łąki Łan", owner: 'Łąki Łan', date: '06-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/255962424790662', status: 'Attend'},
      {id: 2, event:"Rap Festival", owner: 'Spodek Katowice', date: '16-12-2016 16:00', respondUrl: 'https://www.facebook.com/events/1025874204192480', status: 'Respond'}
    ]
   return {facebookShares, facebookPosts, facebookLikes, socials, replies,sharePost, events, logError, addPost };
  }






}
