import {Routes} from '@angular/router';

import {AuthGuardService} from './services/auth-guard.service';

import {DashboardComponent} from './dashboard/dashboard-main/dashboard.component';
import { AddPostComponent } from './addpost/addpost.component';
import { SocialsComponent} from './socials/socials.component';
import { LoginComponent} from './login/login.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
  {path: 'addpost', component: AddPostComponent, canActivate: [AuthGuardService]},
  {path: 'socials', component: SocialsComponent, canActivate: [AuthGuardService]},
  {path: '**', component: PageNotFoundComponent, canActivate: [AuthGuardService]}
];
